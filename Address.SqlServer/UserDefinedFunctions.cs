﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using System.Text.RegularExpressions;

public static partial class UserDefinedFunctions {
    #region Trim

    [SqlFunction(
        DataAccess = DataAccessKind.None,
        IsDeterministic = true,
        IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlString Trim(SqlString input) {
        var output = Regex.Replace(input.Value, @"\s+", " ", RegexOptions.IgnoreCase);
        return new SqlString(output.Trim());
    }

    #endregion
    #region Tokenize

    public struct AddressToken {
        public SqlInt32 Start;
        public SqlInt32 End;
        public SqlInt32 Number;
        public SqlString Name;
        public SqlBoolean IsPostcode;
        public SqlBoolean IsName;
        public SqlBoolean IsBuilding;
    }
    
    [SqlFunction(
        DataAccess = DataAccessKind.None,
        FillRowMethodName = "AddressTokenize_FillRow",
        IsDeterministic = true,
        IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None)]
    public static IEnumerable AddressTokenize(SqlString input, SqlInt32 maxNgramm) {
        const string Digits = @"[0-9]+(-*[а-я]\b)?";
        const string Letters = @"[а-яё]+(-[а-яё]+)?";
        var matches = Regex.Matches(input.Value, String.Format("({0})|({1})", Digits, Letters), RegexOptions.IgnoreCase);
        var tokens = new List<AddressToken>();
        for (int start = 0; start < matches.Count; ++start) {
            var value = matches[start].Value;
            char last = Char.ToUpper(value[value.Length - 1]);
            if (Char.IsLetter(value, 0)
                    || (Char.IsLetter(last) && (last == 'Й' || last == 'Я'))) { // сокращения порядковых числительных 1-й, 2-я и т.д.
                var builder = new StringBuilder();
                for (int offset = 0, end = start;;) {
                    builder.Append(value);
                    tokens.Add(new AddressToken {
                        Start = new SqlInt32(start),
                        End = new SqlInt32(end),
                        Number = SqlInt32.Null,
                        Name = new SqlString(builder.ToString()),
                        IsPostcode = new SqlBoolean(false),
                        IsName = new SqlBoolean(true),
                        IsBuilding = new SqlBoolean(false)
                    });
                    if (++offset < maxNgramm && ++end < matches.Count) {
                        value = matches[start + offset].Value;
                        last = Char.ToUpper(value[value.Length - 1]);
                        if (Char.IsDigit(value, 0) && !(last == 'Й' || last == 'Я')) {
                            break;
                        }
                        builder.Append(" ");
                    }
                    else {
                        break;
                    }
                }
            }
            else {
                Int32 number = Int32.Parse(Regex.Match(value, "[0-9]+", RegexOptions.IgnoreCase).Value);
                bool isPostcode = value.Length == 6 && number >= 100000 && number <= 999999;
                bool isBuilding = number >= 1 && number <= 9999;
                if (isBuilding) {
                    if (Char.IsDigit(last)) {
                        value = number.ToString();
                    }
                    else {
                        value = String.Format("{0}{1}", number, last);
                    }
                }
                else if (!isPostcode) {
                    continue;
                }
                tokens.Add(new AddressToken {
                    Start = new SqlInt32(start),
                    End = new SqlInt32(start),
                    Number = new SqlInt32(number),
                    Name = new SqlString(value),
                    IsPostcode = new SqlBoolean(isPostcode),
                    IsName = new SqlBoolean(false),
                    IsBuilding = new SqlBoolean(isBuilding)
                });
            }
        }
        return tokens;
    }

    public static void AddressTokenize_FillRow(Object tokenObject, out SqlInt32 start, out SqlInt32 end, out SqlInt32 number, out SqlString name, out SqlBoolean isPostcode, out SqlBoolean isName, out SqlBoolean isBuilding) {
        var token = (AddressToken)tokenObject;
        start = token.Start;
        end = token.End;
        number = token.Number;
        name = token.Name;
        isPostcode = token.IsPostcode;
        isName = token.IsName;
        isBuilding = token.IsBuilding;
    }

    #endregion
    #region String Metrics

    private static string Phonetix(string input, int outputLength, out int outputCount) {
        char[] output = new char[outputLength];
        char prevCharOutput = '0';
        int indexOutput = 0;
        for (int indexInput = 0; indexInput < input.Length && indexOutput < outputLength; ++indexInput) {
            char currCharOutput;
            switch (input[indexInput]) {
            case 'б':
            case 'Б':
            case 'п':
            case 'П':
                currCharOutput = '1';
                break;
            case 'в':
            case 'В':
            case 'ф':
            case 'Ф':
            case 'г':
            case 'Г':
            case 'к':
            case 'К':
            case 'х':
            case 'Х':
                currCharOutput = '2';
                break;
            case 'с':
            case 'С':
            case 'з':
            case 'З':
            case 'ц':
            case 'Ц':
                if (prevCharOutput != '4') {
                    currCharOutput = '3';
                }
                else {
                    output[indexOutput - 1] = '3';
                    prevCharOutput = '3';
                    continue;
                }
                break;
            case 'д':
            case 'Д':
            case 'т':
            case 'Т':
                currCharOutput = '4';
                break;
            case 'л':
            case 'Л':
                currCharOutput = '5';
                break;
            case 'м':
            case 'М':
            case 'н':
            case 'Н':
                currCharOutput = '6';
                break;
            case 'р':
            case 'Р':
                currCharOutput = '7';
                break;
            case 'ж':
            case 'Ж':
            case 'ч':
            case 'Ч':
            case 'ш':
            case 'Ш':
            case 'щ':
            case 'Щ':
                currCharOutput = '8';
                break;
            default:
                prevCharOutput = '0';
                continue;
            }
            if (prevCharOutput != currCharOutput) {
                output[indexOutput++] = currCharOutput;
                prevCharOutput = currCharOutput;
            }
        }
        outputCount = indexOutput;
        for (; indexOutput < outputLength; ++indexOutput) {
            output[indexOutput] = '0';
        }
        return new String(output);
    }
/*
    [SqlFunction(
        DataAccess = DataAccessKind.None,
        IsDeterministic = true,
        IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlString Phonetix(SqlString input, SqlInt32 outputLength) {
        int outputCount;
        return new SqlString(Phonetix(input.Value, outputLength.Value, out outputCount));
    }
*/
    [SqlFunction(
        DataAccess = DataAccessKind.None,
        IsDeterministic = true,
        IsPrecise = true,
        SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlDouble Levenstein(SqlString first, SqlString second) {
        if (first.Value.Length <= 0) {
            return new SqlDouble(second.Value.Length > 0 ? 1.0 : 0.0);
        }
        else if (second.Value.Length <= 0) {
            return new SqlDouble(1.0);
        }

        string firstString = first.Value.ToLower();
        string secondString = second.Value.ToLower();
        int[,] distances = new int[firstString.Length + 1, secondString.Length + 1];
        for (int col = 0; col < secondString.Length + 1; ++col) {
            distances[0, col] = col;
        }
        for (int row = 0; row < firstString.Length; ++row) {
            distances[row + 1, 0] = row;
            for (int col = 0; col < secondString.Length; ++col) {
                distances[row + 1, col + 1] = Math.Min(
                    Math.Min(distances[row + 1, col] + 1, distances[row, col + 1] + 1),
                    distances[row, col] + (firstString[row] == secondString[col] ? 0 : 1));
            }
        }
        return new SqlDouble(distances[firstString.Length, secondString.Length] / (double)Math.Max(firstString.Length, secondString.Length));
    }

    #endregion
    #region Computed Columns
/*
    [SqlFunction(
        DataAccess = DataAccessKind.None,
        IsDeterministic = true,
        IsPrecise = false,
        SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlInt32 GetPhoneticName(SqlString name, SqlInt32 outputLength) {
        var match = Regex.Match(name.Value, @"[а-яё]{4,}", RegexOptions.IgnoreCase);
        var phonetix = Phonetix(new SqlString(match.Success ? match.Value : name.Value.Trim()), outputLength);
        return SqlInt32.Parse(phonetix.Value);
    }
*/
    [SqlFunction(
    DataAccess = DataAccessKind.None,
    IsDeterministic = true,
    IsPrecise = false,
    SystemDataAccess = SystemDataAccessKind.None)]
    public static SqlInt32 GetPhoneticName(SqlString name, SqlInt32 outputLength) {
        int count = 0;
        string code = "0";
        foreach (Match match in Regex.Matches(name.Value, @"[а-яё]+(-[а-яё]+)?", RegexOptions.IgnoreCase)) {
            int outputCount;
            var phonetix = Phonetix(match.Value, outputLength.Value, out outputCount);
            if (outputCount > count) {
                count = outputCount;
                code = phonetix;
            }
            if (outputCount == outputLength.Value) {
                break;
            }
        }
        return SqlInt32.Parse(code);
    }

    #endregion
}