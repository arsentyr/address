﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace AddressApplication {
    public class DataGridCellFontStyleConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var isLive = (Nullable<bool>)value;
            return (isLive.HasValue && isLive.Value) ? FontStyles.Normal : FontStyles.Italic;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class DataGridCellBackgroundConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if ((bool)value) {
                return new SolidColorBrush(new Color { A = 0x60, R = 0x00, G = 0x7f, B = 0xff });
            }
            return SystemColors.AppWorkspaceColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class DataGridCellRatingConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var rating = (TestRating)value;
            switch (rating) {
            case TestRating.Fail: return new SolidColorBrush(new Color { A = 0x7d, R = 0xff, G = 0x00, B = 0x00 });
            case TestRating.Good: return new SolidColorBrush(new Color { A = 0x7d, R = 0xff, G = 0xff, B = 0x00 });
            case TestRating.Excellent: return new SolidColorBrush(new Color { A = 0x7d, R = 0x00, G = 0xff, B = 0x00 });
            }
            return SystemColors.AppWorkspaceColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
