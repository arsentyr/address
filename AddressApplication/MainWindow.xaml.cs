﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressApplication {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();

            for (int i = 0; i < 10000; i += 100) {
                cbxSkip.Items.Add(i);
            }
            cbxSkip.SelectedIndex = 0;
            cbxSkip.SelectionChanged += LoadDataGridTest;

            for (int i = 1; i <= 10000; i *= 10) {
                cbxTake.Items.Add(i);
            }
            cbxTake.SelectedIndex = 1;
            cbxTake.SelectionChanged += LoadDataGridTest;

            for (float i = 0.1f; i < 1; i += 0.1f) {
                cbxLimit.Items.Add(i);
            }
            cbxLimit.SelectedIndex = 2;

            for (int i = 10; i <= 500; i += 10) {
                cbxTop.Items.Add(i);
            }
            cbxTop.SelectedIndex = 1;

            LoadDataGridTest(null, null);
        }

        private void LoadDataGridTest(object sender, SelectionChangedEventArgs args) {
            dataGridTest.Items.Clear();   
            using (var context = new AddressTestContext()) {
                var skip = (int)cbxSkip.SelectedValue;
                var take = (int)cbxTake.SelectedValue;
                foreach (var address in context.Addresses.OrderBy(o => o.Id).Skip(skip).Take(take)) {
                    dataGridTest.Items.Add(new AddressTestItem() {
                        Time = String.Empty,
                        Rating = TestRating.None,
                        Id = address.Id,
                        Value = address.Value,
                        Postcode = address.Postcode, 
                        Region = address.Region,
                        RegionIsDetected = false,
                        Area = address.Area,
                        AreaIsDetected = false,
                        Locality = address.Locality,
                        LocalityIsDetected = false,
                        Street = address.Street,
                        ThoroughfareIsDetected = false,
                        Building = address.Building,
                        BuildingIsDetected = false
                    });
                }
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            var address = ((sender as DataGridRow).Item as AddressTestItem);
            tbxParse.Text = address.Value;
        }

        private void btnTestParse_Click(object sender, RoutedEventArgs e) {
            if (tbxParse.Text.Length > 0) {
                btnParse.IsEnabled = false;

                dataGridParse.Items.Clear();
                using (var context = new AddressContext()) {
                    var limit = Double.Parse(cbxLimit.SelectedValue.ToString());
                    var top = (int)cbxTop.SelectedValue;
                    var dateTime = DateTime.Now;
                    var addresses = context.ParseAddress(tbxParse.Text, limit, limit, limit, limit, 3, top).ToList();
                    tbxParseTime.Text = String.Format("parseTime={0}", DateTime.Now.Subtract(dateTime).ToString(@"hh\:mm\:ss"));
                    foreach (var address in addresses) {
                        var region = context.AddressRegions.FirstOrDefault(r => r.Id == address.RegionId) ?? new AddressRegion();
                        var area = context.AddressAreas.FirstOrDefault(a => a.Id == address.AreaId) ?? new AddressArea();
                        var locality = context.AddressLocalities.FirstOrDefault(l => l.Id == address.LocalityId) ?? new AddressLocality();
                        var thoroughfare = context.AddressThoroughfares.FirstOrDefault(t => t.Id == address.ThoroughfareId) ?? new AddressThoroughfare();
                        dataGridParse.Items.Add(new AddressParseItem() {
                            Postcode = address.Postcode,
                            PostcodeIsDetected = address.PostcodeIsDetected,
                            RegionType = region.AddressObjectType == null ? String.Empty : region.AddressObjectType.ShortName,
                            RegionName = region.Name,
                            RegionIsLive = region.IsLive,
                            RegionTypeIsDetected = address.RegionTypeIsDetected,
                            RegionIsDetected = address.RegionIsDetected,
                            AreaType = area.AddressObjectType == null ? String.Empty : area.AddressObjectType.ShortName,
                            AreaName = area.Name,
                            AreaIsLive = area.IsLive,
                            AreaTypeIsDetected = address.AreaTypeIsDetected,
                            AreaIsDetected = address.AreaIsDetected,
                            LocalityType = locality.AddressObjectType == null ? String.Empty : locality.AddressObjectType.ShortName,
                            LocalityName = locality.OfficialName ?? locality.FormalName,
                            LocalityIsLive = locality.IsLive,
                            LocalityTypeIsDetected = address.LocalityTypeIsDetected,
                            LocalityIsDetected = address.LocalityIsDetected,
                            ThoroughfareType = thoroughfare.AddressObjectType == null ? String.Empty : thoroughfare.AddressObjectType.ShortName,
                            ThoroughfareName = thoroughfare.OfficialName ?? thoroughfare.FormalName,
                            ThoroughfareIsLive = thoroughfare.IsLive,
                            ThoroughfareTypeIsDetected = address.ThoroughfareTypeIsDetected,
                            ThoroughfareIsDetected = address.ThoroughfareIsDetected,
                            Building = address.Building,
                            BuildingIsDetected = address.BuildingIsDetected,
                            Relevance = address.Relevance
                        });
                    }
                }
            }  

            btnParse.IsEnabled = true;
        }

        private void btnTest_Click(object sender, RoutedEventArgs e) {
            var testTime = DateTime.Now;
            using (var context = new AddressContext()) {
                int numFail = 0, numGood = 0, numExcellent = 0;
                for (int index = 0; index < dataGridTest.Items.Count; ++index) {
                    var item = (AddressTestItem)dataGridTest.Items[index];
                    if (String.IsNullOrEmpty(item.Region)
                            && String.IsNullOrEmpty(item.Area)
                            && String.IsNullOrEmpty(item.Locality)
                            && String.IsNullOrEmpty(item.Street)) {
                        item.Rating = TestRating.None;
                        continue;
                    }

                    var limit = Double.Parse(cbxLimit.SelectedValue.ToString());
                    var top = (int)cbxTop.SelectedValue;
                    var parseTime = DateTime.Now;
                    var address = context.ParseAddress(item.Value, limit, limit, limit, limit, 3, 1).ToList().FirstOrDefault();

                    item.Time = DateTime.Now.Subtract(parseTime).ToString(@"mm\:ss");
                    item.PostcodeIsDetected = item.Postcode != null && item.Postcode.Equals(address.Postcode.HasValue ? address.Postcode.Value.ToString() : String.Empty);
                    item.RegionIsDetected = address.RegionName != null && address.RegionName.Equals(item.Region, StringComparison.CurrentCultureIgnoreCase);
                    item.AreaIsDetected = address.AreaName != null && address.AreaName.Equals(item.Area, StringComparison.CurrentCultureIgnoreCase);
                    item.LocalityIsDetected = address.LocalityName != null && address.LocalityName.Equals(item.Locality, StringComparison.CurrentCultureIgnoreCase);
                    item.ThoroughfareIsDetected = address.ThoroughfareName != null && address.ThoroughfareName.Equals(item.Street, StringComparison.CurrentCultureIgnoreCase);
                    item.BuildingIsDetected = address.Building != null && address.Building.Equals(item.Building, StringComparison.CurrentCultureIgnoreCase);
                    if (!item.RegionIsDetected
                            && !item.AreaIsDetected
                            && !item.LocalityIsDetected
                            && !item.ThoroughfareIsDetected) {
                        item.Rating = TestRating.Fail;
                        ++numFail;
                    }
                    else if ((item.PostcodeIsDetected || String.IsNullOrEmpty(item.Postcode))
                            && (item.RegionIsDetected || String.IsNullOrEmpty(item.Region))
                            && (item.AreaIsDetected || String.IsNullOrEmpty(item.Area))
                            && (item.LocalityIsDetected || String.IsNullOrEmpty(item.Locality))
                            && (item.ThoroughfareIsDetected || String.IsNullOrEmpty(item.Street))
                            && (item.BuildingIsDetected || String.IsNullOrEmpty(item.Building))) {
                        item.Rating = TestRating.Excellent;
                        ++numExcellent;
                    }
                    else {
                        item.Rating = TestRating.Good;
                        ++numGood;
                    }
                }
                dataGridTest.Items.Refresh();
                tbxTestTime.Text = String.Format("testTime={0}", DateTime.Now.Subtract(testTime).ToString(@"hh\:mm\:ss"));
                float sum = (float)(numFail + numGood + numExcellent);
                tbxTestFail.Text = String.Format("Fail: {0:F1}%", numFail / sum * 100);
                tbxTestGood.Text = String.Format("Good: {0:F1}%", numGood / sum * 100);
                tbxTestExcellent.Text = String.Format("Excellent: {0:F1}%", numExcellent / sum * 100);
            }
        }
    }

    public class AddressParseItem {
        public int? Postcode { set; get; }
        public bool PostcodeIsDetected { set; get; }
        public string RegionType { set; get; }
        public string RegionName { set; get; }
        public bool RegionIsLive { set; get; }
        public bool? RegionTypeIsDetected { set; get; }
        public bool RegionIsDetected { set; get; }
        public string AreaType { set; get; }
        public string AreaName { set; get; }
        public bool AreaIsLive { set; get; }
        public bool? AreaTypeIsDetected { set; get; }
        public bool AreaIsDetected { set; get; }
        public string LocalityType { set; get; }
        public string LocalityName { set; get; }
        public bool LocalityIsLive { set; get; }
        public bool? LocalityTypeIsDetected { set; get; }
        public bool LocalityIsDetected { set; get; }
        public string ThoroughfareType { set; get; }
        public string ThoroughfareName { set; get; }
        public bool ThoroughfareIsLive { set; get; }
        public bool? ThoroughfareTypeIsDetected { set; get; }
        public bool ThoroughfareIsDetected { set; get; }
        public string Building { set; get; }
        public bool BuildingIsDetected { set; get; }
        public double Relevance { set; get; }
    }

    public enum TestRating {
        None,
        Fail,
        Good,
        Excellent
    }

    public class AddressTestItem {
        public string Time { set; get; }
        public TestRating Rating { set; get; }
        public int Id { set; get; }
        public string Value { set; get; }
        public string Postcode { set; get; }
        public bool PostcodeIsDetected { set; get; }
        public string Region { set; get; }
        public bool RegionIsDetected { set; get; }
        public string Area { set; get; }
        public bool AreaIsDetected { set; get; }
        public string Locality { set; get; }
        public bool LocalityIsDetected { set; get; }
        public string Street { set; get; }
        public bool ThoroughfareIsDetected { set; get; }
        public string Building { set; get; }
        public bool BuildingIsDetected { set; get; }
    }
}
