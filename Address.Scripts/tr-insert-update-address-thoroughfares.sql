-- ������� �� ���������� � ���������� ������� � ������� ��������������

USE [work_db]
GO

-- �������� ������������� ��������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'TR' AND name = N'InsertUpdate_AddressThoroughfares')
	DROP TRIGGER [dbo].[InsertUpdate_AddressThoroughfares];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� ��������
CREATE TRIGGER [dbo].[InsertUpdate_AddressThoroughfares]
ON [dbo].[AddressThoroughfares]
AFTER INSERT, UPDATE 
AS
BEGIN
SET NOCOUNT ON;

-- ��� ����������� �������, ���� ���������� ������, �� �.�. ������ �������
IF EXISTS(
	SELECT *
	FROM inserted AS [in]
	INNER JOIN deleted AS [out]
		ON [in].[Guid] = [out].[Guid]
		AND [in].[UpdateDate] < [out].[UpdateDate])
BEGIN
	RAISERROR(N'��� ����������� �������, ���� ���������� ������, �� ����� ���� ������ �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ������ ������ ����� ���, ���������� ��� ��������������
IF EXISTS(
	SELECT [TypeId]
	FROM inserted
	GROUP BY [TypeId]
	HAVING [TypeId] NOT IN (
		SELECT [type].[Id]
		FROM (
			SELECT [Id]
			FROM [dbo].[AddressObjectLevels]
			WHERE [Name] = N'��������������') AS [level]
		INNER JOIN [dbo].[AddressObjectTypes] AS [type]
			ON [level].[Id] = [type].[LevelId]))
BEGIN
	RAISERROR(N'������ ������ ����� ���, ���������� ��� ��������������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- �.�. �� ����� ������ �������������� ����������� ��������������, � ������� � ��������������
IF EXISTS(
	SELECT [ObjectGuid] 
	FROM [dbo].[AddressThoroughfares]
	WHERE [IsLive] = 0 AND [IsActual] = 1
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ �������������� ����������� ��������������, � ������� � ���������������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- �.�. �� ����� ������ ������������ ��������������, � ������� � ��������������
IF EXISTS(
	SELECT [ObjectGuid]
	FROM [dbo].[AddressThoroughfares]
	WHERE [IsLive] = 1
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ ������������ ��������������, � ������� � ���������������.', 10, 1);
	ROLLBACK;
	RETURN
END

DECLARE @count int = (SELECT count(*) FROM inserted); -- ����� ����������� ��������������

-- ������� ����� ��������� �� ��������
SELECT @count -= count(*)
FROM inserted AS [thoroughfare]
WHERE [AreaId] IS NULL
AND [LocalityId] IS NULL
AND [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressRegions] AS [parent]
	WHERE [thoroughfare].[RegionId] = [parent].[Id]
	AND [thoroughfare].[ParentGuid] = [parent].[Guid] 
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- ������� ����� ��������� �� �������
SELECT @count -= count(*)
FROM inserted AS [thoroughfare]
WHERE [LocalityId] IS NULL
AND [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressAreas] AS [parent]
	WHERE [thoroughfare].[AreaId] = [parent].[Id]
	AND [thoroughfare].[ParentGuid] = [parent].[Guid]
	AND [thoroughfare].[RegionId] = [parent].[RegionId]
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- ������� ����� ��������� �� ��������� �������
SELECT @count -= count(*)
FROM inserted AS [thoroughfare]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressLocalities] AS [parent]
	WHERE [thoroughfare].[LocalityId] = [parent].[Id]
	AND [thoroughfare].[ParentGuid] = [parent].[Guid]
	AND [thoroughfare].[RegionId] = [parent].[RegionId]
	AND (([thoroughfare].[AreaId] IS NULL AND [parent].[AreaId] IS NULL)
		OR [thoroughfare].[AreaId] = [parent].[AreaId])
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- ������� ����� ��������� �� ��������������
SELECT @count -= count(*)
FROM inserted AS [thoroughfare]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressThoroughfares] AS [parent]
	WHERE [thoroughfare].[ParentGuid] = [parent].[Guid]
	AND [thoroughfare].[RegionId] = [parent].[RegionId]
	AND (([thoroughfare].[AreaId] IS NULL AND [parent].[AreaId] IS NULL)
		OR [thoroughfare].[AreaId] = [parent].[AreaId])
	AND (([thoroughfare].[LocalityId] IS NULL AND [parent].[LocalityId] IS NULL)
		OR [thoroughfare].[LocalityId] = [parent].[LocalityId])
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- �������� �� ��������� ����� ����������� ������� � ������ �� ���������
IF (@count <> 0)
BEGIN
	RAISERROR(N'������������ ������, ��� ����������� ��������������, ������ ������������ � ���� ������ � ���� ������������ ��� �����������.', 10, 1);
	ROLLBACK;
	RETURN
END

END
GO