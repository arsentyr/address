USE [work_db]
GO

ALTER TABLE [dbo].[AddressRegions]
DROP COLUMN [PhoneticName];

ALTER TABLE [dbo].[AddressAreas]
DROP COLUMN [PhoneticName];

ALTER TABLE [dbo].[AddressLocalities]
DROP COLUMN [PhoneticFormalName];

ALTER TABLE [dbo].[AddressLocalities]
DROP COLUMN [PhoneticOfficialName];

ALTER TABLE [dbo].[AddressThoroughfares]
DROP COLUMN [PhoneticFormalName];

ALTER TABLE [dbo].[AddressThoroughfares]
DROP COLUMN [PhoneticOfficialName];

DROP FUNCTION [dbo].[Trim];
DROP FUNCTION [dbo].[AddressTokenize];
DROP FUNCTION [dbo].[Levenstein];
DROP FUNCTION [dbo].[GetPhoneticName];

DROP ASSEMBLY [Address];
GO

SET NUMERIC_ROUNDABORT OFF;
SET ANSI_NULLS ON;
SET ANSI_PADDING ON;
SET ANSI_WARNINGS ON;
SET ARITHABORT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
SET QUOTED_IDENTIFIER ON;
GO

CREATE ASSEMBLY [Address]
FROM '..\..\Address.SqlServer\bin\Release\Address.SqlServer.dll'
WITH PERMISSION_SET = SAFE
GO

CREATE FUNCTION Trim(@input nvarchar(max))
RETURNS nvarchar(max)
AS EXTERNAL NAME [Address].[UserDefinedFunctions].[Trim]
GO

CREATE FUNCTION AddressTokenize(@input nvarchar(max), @maxNgramm int)
RETURNS table (
	[Start] int,
	[End] int,
	[Number] int,
	[Name] nvarchar(max),
	[IsPostcode] bit,
	[IsName] bit,
	[IsBuilding] bit)
AS EXTERNAL NAME [Address].[UserDefinedFunctions].[AddressTokenize]
GO

CREATE FUNCTION Levenstein(@first nvarchar(max), @second nvarchar(max))
RETURNS float
AS EXTERNAL NAME [Address].[UserDefinedFunctions].[Levenstein]
GO

CREATE FUNCTION GetPhoneticName(@name nvarchar(max), @outputLength int)
RETURNS int
AS EXTERNAL NAME [Address].[UserDefinedFunctions].[GetPhoneticName]
GO

ALTER TABLE [dbo].[AddressRegions]
ADD [PhoneticName] AS (dbo.GetPhoneticName([Name], 4)) PERSISTED

ALTER TABLE [dbo].[AddressAreas]
ADD [PhoneticName] AS (dbo.GetPhoneticName([Name], 4)) PERSISTED

ALTER TABLE [dbo].[AddressLocalities]
ADD [PhoneticFormalName] AS (dbo.GetPhoneticName([FormalName], 4)) PERSISTED

ALTER TABLE [dbo].[AddressLocalities]
ADD [PhoneticOfficialName] AS (dbo.GetPhoneticName(ISNULL([OfficialName], ''), 4)) PERSISTED

ALTER TABLE [dbo].[AddressThoroughfares]
ADD [PhoneticFormalName] AS (dbo.GetPhoneticName([FormalName], 4)) PERSISTED
GO

ALTER TABLE [dbo].[AddressThoroughfares]
ADD [PhoneticOfficialName] AS (dbo.GetPhoneticName(ISNULL([OfficialName], ''), 4)) PERSISTED
GO