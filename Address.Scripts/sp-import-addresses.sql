-- �������� ��������� ������� ������� �� �� ����

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddresses')
	DROP PROCEDURE [dbo].[ImportAddresses];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddresses]
	@fiasAddressObjectTypesDbf nvarchar(max),
	@fiasAddressObjectsDbf nvarchar(max),
	@fiasBuildingsIntervalsDbf nvarchar(max) 
AS
BEGIN

EXEC dbo.ImportAddressObjectTypes @fiasAddressObjectTypesDbf;
IF @@ERROR <> 0 RETURN;

EXEC dbo.ImportAddressRegions @fiasAddressObjectsDbf;
IF @@ERROR <> 0 RETURN;

EXEC dbo.ImportAddressAreas @fiasAddressObjectsDbf;
IF @@ERROR <> 0 RETURN;

EXEC dbo.ImportAddressLocalities @fiasAddressObjectsDbf;
IF @@ERROR <> 0 RETURN;

EXEC dbo.ImportAddressThoroughfares @fiasAddressObjectsDbf;
IF @@ERROR <> 0 RETURN;

EXEC dbo.ImportAddressBuildingsIntervals @fiasBuildingsIntervalsDbf;

END
GO