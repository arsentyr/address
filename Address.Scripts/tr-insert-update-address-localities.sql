-- ������� �� ���������� � ���������� ������� � ������� ��������� ������� (��)

USE [work_db]
GO

-- �������� ������������� ��������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'TR' AND name = N'InsertUpdate_AddressLocalities')
	DROP TRIGGER [dbo].[InsertUpdate_AddressLocalities];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� ��������
CREATE TRIGGER [dbo].[InsertUpdate_AddressLocalities]
ON [dbo].[AddressLocalities]
AFTER INSERT, UPDATE 
AS
BEGIN
SET NOCOUNT ON;

-- ��� ����������� �������, ���� ���������� ������, �� �.�. ������ �������
IF EXISTS(
	SELECT *
	FROM inserted AS [in]
	INNER JOIN deleted AS [out]
		ON [in].[Guid] = [out].[Guid]
		AND [in].[UpdateDate] < [out].[UpdateDate])
BEGIN
	RAISERROR(N'��� ����������� �������, ���� ���������� ������, �� ����� ���� ������ �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ������ ������ ����� ���, ���������� ��� ��
IF EXISTS(
	SELECT [TypeId]
	FROM inserted
	GROUP BY [TypeId]
	HAVING [TypeId] NOT IN (
		SELECT [type].[Id]
		FROM (
			SELECT [Id]
			FROM [dbo].[AddressObjectLevels]
			WHERE [Name] = N'��������� �����') AS [level]
		INNER JOIN [dbo].[AddressObjectTypes] AS [type]
			ON [level].[Id] = [type].[LevelId]))
BEGIN
	RAISERROR(N'������ ������ ����� ���, ���������� ��� ���������� ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- �.�. �� ����� ������ �������������� ����������� ��, � ������� � ��
IF EXISTS(
	SELECT [ObjectGuid] 
	FROM [dbo].[AddressLocalities]
	WHERE [IsLive] = 0 AND [IsActual] = 1
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ �������������� ����������� ���������� ������, � ������� � ��������� ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ����������� ��
DECLARE @localities table (
	[ObjectGuid] uniqueidentifier not null,
	[Postcode] int null
);
INSERT INTO @localities
SELECT
	[ObjectGuid],
	[Postcode]
FROM [dbo].[AddressLocalities]
WHERE [IsLive] = 1;

-- �.�. �� ����� ������ ������������ ��, � ������� � ��
IF EXISTS(
	SELECT [ObjectGuid]
	FROM @localities
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ ������������ ���������� ������, � ������� � ��������� ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ��� �������, �� ����������� ��, ��� �������� ������ �.�. ����������
IF EXISTS(
	SELECT [Postcode] 
	FROM @localities
	WHERE [Postcode] IS NOT NULL
	GROUP BY [ObjectGuid], [Postcode]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'��� �������, �� ����������� ��������� �������, ��� �������� ������ ������ ���� ����������.', 10, 1);
	ROLLBACK;
	RETURN
END

DECLARE @count int = (SELECT count(*) FROM inserted); -- ����� ����������� ��

-- ������� ����� ��������� �� ��������
SELECT @count -= count(*)
FROM inserted AS [locality]
WHERE [AreaId] IS NULL
AND [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressRegions] AS [parent]
	WHERE [locality].[RegionId] = [parent].[Id]
	AND [locality].[ParentGuid] = [parent].[Guid] 
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- ������� ����� ��������� �� �������
SELECT @count -= count(*)
FROM inserted AS [locality]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressAreas] AS [parent]
	WHERE [locality].[AreaId] = [parent].[Id]
	AND [locality].[ParentGuid] = [parent].[Guid]
	AND [locality].[RegionId] = [parent].[RegionId]
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- �����	�� ����� ��������� �� ��
SELECT @count -= count(*)
FROM inserted AS [locality]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressLocalities] AS [parent]
	WHERE [locality].[ParentGuid] = [parent].[Guid]
	AND [locality].[RegionId] = [parent].[RegionId]
	AND (([locality].[AreaId] IS NULL AND [parent].[AreaId] IS NULL)
		OR [locality].[AreaId] = [parent].[AreaId])
	AND ([parent].[IsLive] = 1 OR [parent].[IsActual] = 1));

-- �������� �� ��������� ����� ����������� ������� � ������ �� ���������
IF (@count <> 0)
BEGIN
	RAISERROR(N'������������ ������, ��� ����������� ��������� �������, ������ ������������ � ���� ������ � ���� ������������ ��� �����������.', 10, 1);
	ROLLBACK;
	RETURN
END

RETURN
END
GO