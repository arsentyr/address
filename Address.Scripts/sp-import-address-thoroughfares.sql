-- �������� ��������� ������� ������� �� ������, �������������� �����������
-- � �������� �������������� ����������

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressThoroughfares')
	DROP PROCEDURE [dbo].[ImportAddressThoroughfares];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressThoroughfares]
	@fiasAddressObjectsDbf nvarchar(max) -- ���� � dbf-����� �� ���� 
AS
BEGIN
SET NOCOUNT ON;

-- ������ �������� �������� (��) �� ���� 
DECLARE
	@fiasStreet int = 7,						-- ������� �����
	@fiasAdditionalTerritory int = 90,			-- ������� �������������� ����������
	@fiasSubjectAdditionalTerritory int = 91;	-- ������� ��������, ����������� �������������� �����������

-- ������ �������������� �� dbf-����� �� ����
DECLARE @thoroughfares table (
	[Guid] uniqueidentifier not null,		-- guid ������ �� ��
	[ObjectGuid] uniqueidentifier not null, -- guid ��
	[ParentGuid] uniqueidentifier not null, -- guid �������� ��
	[ParentLevelId] int null,				-- ������� �������� ��
	[RegionId] int null,					-- id ������� ��
	[AreaId] int null,						-- id ������ ��
	[LocalityId] int null,					-- id ���������� ������ ��
	[TypeId] int null,						-- id ���� ��
	[TypeShortName] nvarchar(10) not null,	-- ������� ������������ ���� ��
	[FormalName] nvarchar(120) not null,	-- ���������� ������������ ��
	[OfficialName] nvarchar(120) null,		-- ����������� ������������ ��
	[Postcode] nvarchar(6) null,			-- �������� ������ ��
	[IsLive] bit not null,					-- ������� ������������ ��
	[IsActual] bit not null,				-- ������� ������������ ��
	[UpdateDate] date not null,				-- ���� ���������� ������ �� ��
	[PrevGuid] uniqueidentifier null,		-- guid ���������� ������ �� ��
	[Updated] bit null);
INSERT INTO @thoroughfares (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[TypeShortName],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate],
	[PrevGuid])
EXEC(N'
SELECT
	convert(uniqueidentifier, [source].[AOID]) AS [Guid],
	convert(uniqueidentifier, [source].[AOGUID]) AS [ObjectGuid],
	convert(uniqueidentifier, [source].[PARENTGUID]) AS [ParentGuid],
	lower(dbo.Trim([source].[SHORTNAME])) AS [TypeShortName],
	dbo.Trim([source].[FORMALNAME]) AS [FormalName],
	CASE 
		WHEN [source].[OFFNAME] IS NULL
			OR dbo.Trim([source].[OFFNAME]) = dbo.Trim([source].[FORMALNAME]) THEN NULL
		ELSE dbo.Trim([source].[OFFNAME])
	END AS [OfficialName],
	[source].[POSTALCODE] AS [Postcode],
	[source].[LIVESTATUS] AS [IsLive],
	[source].[ACTSTATUS] AS [IsActual],
	convert(date, [source].[UPDATEDATE]) AS [UpdateDate],
	convert(uniqueidentifier, [source].[PREVID]) AS [PrevGuid]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		BackgroundFetch=No;
		Deleted=No;
		Exclusive=No;
		Null=No;
		SourceType=DBF;'',
	''SELECT
		[AOID],
		[AOGUID],
		[PARENTGUID],
		[AOLEVEL],
		[SHORTNAME],
		[FORMALNAME],
		[OFFNAME],
		[POSTALCODE],
		[LIVESTATUS],
		[ACTSTATUS],
		[UPDATEDATE],
		[PREVID]
	  FROM ' + @fiasAddressObjectsDbf + '
	  WHERE [AOLEVEL] IN (' + @fiasStreet
		+ ', ' + @fiasAdditionalTerritory
		+ ', ' + @fiasSubjectAdditionalTerritory + ')'') AS [source]');

-- Id ������� �� �� ��
DECLARE
	@addressRegionLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'),				-- id ������ �������
	@addressAreaLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'),					-- id ������ ������
	@addressLocalityLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����'),	-- id ������ ���������� ������
	@addressThoroughfareLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������');	-- id ������ ��������������

-- ���������� ���� ��
UPDATE [thoroughfare]
SET [TypeId] = [type].[Id]
FROM @thoroughfares AS [thoroughfare]
INNER JOIN (
		SELECT [Id], [ShortName]
		FROM [dbo].[AddressObjectTypes]
		WHERE [LevelId] = @addressThoroughfareLevelId) AS [type]
	ON [thoroughfare].[TypeShortName] = [type].[ShortName];

-- ���������� �������� �� �� �������� � ��
UPDATE [thoroughfare]
SET	[ParentLevelId] = @addressRegionLevelId,
	[ParentGuid] = [region].[Guid],
	[RegionId] = [region].[Id]
FROM @thoroughfares AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressRegions]
		WHERE [IsLive] = 1) AS [region] -- ������� ������� �� ����������� ��������
	ON [thoroughfare].[ParentGuid] = [region].[ObjectGuid]; 

UPDATE [thoroughfare]
SET	[ParentLevelId] = @addressRegionLevelId,
	[ParentGuid] = [region].[Guid],
	[RegionId] = [region].[Id]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressRegions]
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [region]	-- ����� ������� ������ �� ���������� ��������
	ON [thoroughfare].[ParentGuid] = [region].[ObjectGuid];

-- ���������� �������� �� �� ������� � ��
UPDATE [thoroughfare]
SET [ParentLevelId] = @addressAreaLevelId,
	[ParentGuid] = [area].[Guid],
	[RegionId] = [area].[RegionId],
	[AreaId] = [area].[Id]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL) AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressAreas]
		WHERE [IsLive] = 1) AS [area] -- ������� ������� �� ����������� �������
	ON [thoroughfare].[ParentGuid] = [area].[ObjectGuid];

UPDATE [thoroughfare]
SET [ParentLevelId] = @addressAreaLevelId,
	[ParentGuid] = [area].[Guid],
	[RegionId] = [area].[RegionId],
	[AreaId] = [area].[Id]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressAreas] 
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [area] -- ����� ������� ������ �� ���������� �������
	ON [thoroughfare].[ParentGuid] = [area].[ObjectGuid]; 

-- ���������� �������� �� �� ��������� ������� � �� 
UPDATE [thoroughfare]
SET [ParentLevelId] = @addressLocalityLevelId,
	[ParentGuid] = [locality].[Guid],
	[RegionId] = [locality].[RegionId],
	[AreaId] = [locality].[AreaId],
	[LocalityId] = [locality].[Id]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL) AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressLocalities]
		WHERE [IsLive] = 1) AS [locality] -- ������� ������� �� ����������� ��������� �������
	ON [thoroughfare].[ParentGuid] = [locality].[ObjectGuid]; 

UPDATE [thoroughfare]
SET [ParentLevelId] = @addressLocalityLevelId,
	[ParentGuid] = [locality].[Guid],
	[RegionId] = [locality].[RegionId],
	[AreaId] = [locality].[AreaId],
	[LocalityId] = [locality].[Id]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [thoroughfare]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressLocalities]
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [locality] -- ����� ������� ������ �� ���������� ��������� �������
	ON [thoroughfare].[ParentGuid] = [locality].[ObjectGuid];

-- ���������� �������� �� �� ��������������, ����������� � ������������ � �� 
UPDATE [thoroughfare]
SET [ParentLevelId] = @addressThoroughfareLevelId,
	[ParentGuid] = [parent].[Guid],
	[RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId],
	[LocalityId] = [parent].[LocalityId]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL) AS [thoroughfare]
INNER JOIN (
		SELECT [Guid], [ObjectGuid], [IsLive], [RegionId], [AreaId], [LocalityId]
		FROM [dbo].[AddressThoroughfares]
		WHERE [IsLive] = 1 AND [Guid] NOT IN (SELECT [Guid] FROM @thoroughfares)
		UNION ALL
		SELECT [Guid], [ObjectGuid], [IsLive], [RegionId], [AreaId], [LocalityId]
		FROM @thoroughfares
		WHERE [IsLive] = 1) AS [parent] -- ������� ������� �� ����������� ��������������
	ON [thoroughfare].[ParentGuid] = [parent].[ObjectGuid];

UPDATE [thoroughfare]
SET [ParentLevelId] = @addressThoroughfareLevelId,
	[ParentGuid] = [parent].[Guid],
	[RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId],
	[LocalityId] = [parent].[LocalityId]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [thoroughfare]
INNER JOIN (
		SELECT [Guid], [ObjectGuid], [IsActual], [RegionId], [AreaId], [LocalityId]
		FROM [dbo].[AddressThoroughfares]
		WHERE [IsActual] = 1 AND [IsLive] = 0 AND [Guid] NOT IN (SELECT [Guid] FROM @thoroughfares)
		UNION ALL
		SELECT [Guid], [ObjectGuid], [IsActual], [RegionId], [AreaId], [LocalityId]
		FROM @thoroughfares
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [parent] -- ����� ������� ������ �� ���������� ��������������
	ON [thoroughfare].[ParentGuid] = [parent].[ObjectGuid];

-- ���������� �������, ������ � ���������� ������ ��� �������������� � ������� �������� ���� ��������������
DECLARE @parents table (
	[Guid] uniqueidentifier not null,
	[ObjectGuid] uniqueidentifier not null,
	[ParentGuid] uniqueidentifier not null,
	[ParentLevelId] int not null,
	[RegionId] int null,
	[AreaId] int null,
	[LocalityId] int null);
INSERT INTO @parents
SELECT [Guid], [ObjectGuid], [ParentGuid], [ParentLevelId], [RegionId], [AreaId], [LocalityId]
FROM [dbo].[AddressThoroughfares]
WHERE [Guid] NOT IN (SELECT [Guid] FROM @thoroughfares)
UNION ALL
SELECT [Guid], [ObjectGuid], [ParentGuid], [ParentLevelId], [RegionId], [AreaId], [LocalityId]
FROM @thoroughfares;

UPDATE [thoroughfare]
SET [RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId],
	[LocalityId] = [parent].[LocalityId]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [ParentLevelId] = @addressThoroughfareLevelId
	AND [RegionId] IS NULL) AS [thoroughfare]
INNER JOIN @parents AS [parent]
	ON [parent].[Guid] = ( -- ����������� 3 ������ �������������: ����� -> �������������� ���������� -> ������ �������������� ����������
		SELECT CASE
			WHEN [additionalTerritory].[ParentLevelId] IN (@addressRegionLevelId, @addressAreaLevelId, @addressLocalityLevelId)
				THEN [additionalTerritory].[Guid]
			WHEN [additionalTerritory].[ParentLevelId] = @addressThoroughfareLevelId THEN (
				SELECT [street].[Guid]
				FROM @parents AS [street]
				WHERE [additionalTerritory].[ParentGuid] = [street].[Guid]
				AND [street].[ParentLevelId] IN (@addressRegionLevelId, @addressAreaLevelId, @addressLocalityLevelId))
			END
		FROM @parents AS [additionalTerritory]
		WHERE [thoroughfare].[ParentGuid] = [additionalTerritory].[Guid]);

-- ���������� ���������� � ���������� ������� � ��������������� � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- �����������, ����������������� � �� ����, �������� �������� ������������ ��������������
UPDATE [dbo].[AddressThoroughfares]
SET [IsLive] = 0 -- �������� � 0 ������� ������������ ��,
FROM [dbo].[AddressThoroughfares] AS [destination] -- ��� �������������� �� ��, �������
INNER JOIN @thoroughfares AS [source]
	ON [destination].[ObjectGuid] = [source].[ObjectGuid] -- ����� ����� ������ �� �� �
	AND [destination].[Guid] <> [source].[Guid] -- guid ������� �� ��������� �
	AND [destination].[IsLive] = 1 AND [source].[IsLive] = 1 -- ��� �� �������� ������������ �
	AND [source].[PrevGuid] IS NOT NULL
	AND [destination].[Guid] = [source].[PrevGuid]; -- ������ �� �� � ��, �������� ���������� ������������ ����� ������

PRINT N'����������: ' + str(@@ROWCOUNT);

UPDATE @thoroughfares
SET [Updated] = CASE
		WHEN EXISTS(
			SELECT *
			FROM [dbo].[AddressThoroughfares] AS [destination]
			WHERE [source].[Guid] = [destination].[Guid]) THEN 1
		ELSE 0
	END
FROM @thoroughfares AS [source];

-- ����������, �������������� � ��, ��������������
INSERT INTO [dbo].[AddressThoroughfares] (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[ParentLevelId],
	[RegionId],
	[AreaId],
	[LocalityId],
	[TypeId],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate])
SELECT
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[ParentLevelId],
	[RegionId],
	[AreaId],
	[LocalityId],
	[TypeId],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate]
FROM @thoroughfares
WHERE [Updated] = 0;

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- ����������, ������������ � ��, ��������������
UPDATE [dbo].[AddressThoroughfares]
SET [ObjectGuid] = [source].[ObjectGuid],
	[ParentGuid] = [source].[ParentGuid],
	[ParentLevelId] = [source].[ParentLevelId],
	[RegionId] = [source].[RegionId],
	[AreaId] = [source].[AreaId],
	[LocalityId] = [source].[LocalityId],
	[TypeId] = [source].[TypeId],
	[FormalName] = [source].[FormalName],
	[OfficialName] = [source].[OfficialName],
	[Postcode] = [source].[Postcode],
	[IsLive] = [source].[IsLive],
	[IsActual] = [source].[IsActual],
	[UpdateDate] = [source].[UpdateDate]
FROM (
	SELECT *
	FROM @thoroughfares
	WHERE [Updated] = 1) AS [source]
INNER JOIN [dbo].[AddressThoroughfares] AS [destination]
	ON [source].[Guid] = [destination].[Guid];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @thoroughfares);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ �������������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ �������������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO