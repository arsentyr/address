-- �������� ��������� ������� ������� � ����� �������� �������� (��) �� �� ���� 

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressObjectTypes')
	DROP PROCEDURE [dbo].[ImportAddressObjectTypes];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressObjectTypes]
	@fiasAddressObjectTypesDbf nvarchar(max)
AS
BEGIN
SET NOCOUNT ON;

-- ������ �� �� ����
DECLARE
	@fiasRegion int = 1,						-- ������� �������
	@fiasArea int = 3,							-- ������� ������
	@fiasCity int = 4,							-- ������� ������
	@fiasIntracityTerritory int = 5,			-- ������� ��������������� ����������
	@fiasLocality int = 6,						-- ������� ���������� ������
	@fiasStreet int = 7,						-- ������� �����
	@fiasAdditionalTerritory int = 90,			-- ������� �������������� ����������
	@fiasSubjectAdditionalTerritory int = 91;	-- ������� ��������, ����������� �������������� �����������

-- Id ������� �� �� ��
DECLARE
	@addressRegion int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'),				-- id ������ �������
	@addressArea int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'),					-- id ������ ������
	@addressLocality int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����'),	-- id ������ ���������� ������
	@addressThoroughfare int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������');	-- id ������ ��������������

-- ������ ����� �� �� dbf-����� �� ����
DECLARE @types table (
	[LevelId] int not null,		-- id ������ ��
	[ShortName] nvarchar(10),	-- ������� ������������ ���� ��
	[FullName] nvarchar(50));	-- ������ ������������ ���� ��
INSERT INTO @types (
	[LevelId],
	[ShortName],
	[FullName])
EXEC(N'
SELECT DISTINCT
	CASE [LEVEL]
		WHEN ' + @fiasRegion + ' THEN ' + @addressRegion + '
		WHEN ' + @fiasArea + ' THEN ' + @addressArea + '
		WHEN ' + @fiasCity + ' THEN ' + @addressLocality + '
		WHEN ' + @fiasIntracityTerritory + ' THEN ' + @addressLocality + '
		WHEN ' + @fiasLocality + ' THEN ' + @addressLocality + '
		WHEN ' + @fiasStreet + ' THEN ' + @addressThoroughfare + '
		WHEN ' + @fiasAdditionalTerritory + ' THEN ' + @addressThoroughfare + '
		WHEN ' + @fiasSubjectAdditionalTerritory + ' THEN ' + @addressThoroughfare + '
	END AS [LevelId],
	lower(dbo.Trim([SCNAME])) AS [ShortName],
	lower(dbo.Trim([SOCRNAME])) AS [FullName]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		SourceType=DBF;
		Deleted=No;
		BackgroundFetch=No;
		Exclusive=No;
		Null=No;'',
	''SELECT [LEVEL], [SCNAME], [SOCRNAME]
	  FROM ' + @fiasAddressObjectTypesDbf + ''')
WHERE [LEVEL] IN (' 
	+ @fiasRegion + ', '
	+ @fiasArea + ', '
	+ @fiasCity + ', '
	+ @fiasIntracityTerritory + ', '
	+ @fiasLocality + ', '
	+ @fiasStreet + ', '
	+ @fiasAdditionalTerritory + ','
	+ @fiasSubjectAdditionalTerritory + ')');

-- �������� ������������ ����� �� �� ������������� �������
DECLARE @ignores table ([FullName] nvarchar(50) not null);
INSERT INTO @ignores ([FullName])
VALUES
	(N'�������'),
	(N'�������-������������ ��������'),
	(N'������� ������������');

DELETE FROM @types
WHERE [FullName] IN (SELECT [FullName] FROM @ignores);

-- ���������� ���������� � ���������� �������, � ����� ��, � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- ����������, ������������ � ��, ����� ��
UPDATE [dbo].[AddressObjectTypes]
SET [FullName] = [source].[FullName]
FROM [dbo].[AddressObjectTypes] AS [destination]
INNER JOIN @types AS [source]
	ON [destination].[LevelId] = [source].[LevelId]
	AND [destination].[ShortName] = [source].[ShortName];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- ����������, �������������� � ��, ����� ��
INSERT INTO [dbo].[AddressObjectTypes] (
	[LevelId],
	[ShortName],
	[FullName])
SELECT 
	[LevelId],
	[ShortName],
	[FullName]
FROM @types AS [source]
WHERE NOT EXISTS(
	SELECT *
	FROM [dbo].[AddressObjectTypes] AS [destination]
	WHERE [destination].[LevelId] = [source].[LevelId]
	AND [destination].[ShortName] = [source].[ShortName])
ORDER BY [LevelId], [ShortName], [FullName];

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @types);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ ����� �������� �������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ ����� �������� �������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO