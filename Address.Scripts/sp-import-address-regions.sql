-- �������� ��������� ������� ������� � �������� �� �� ���� 

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressRegions')
	DROP PROCEDURE [dbo].[ImportAddressRegions];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressRegions]
	@fiasAddressObjectsDbf nvarchar(max)  -- ���� � dbf-����� �� ����
AS
BEGIN
SET NOCOUNT ON;

-- ������ �������� �������� (��) �� ����
DECLARE @fiasRegionLevel int = 1;	-- ������� �������

-- ������ �������� �� dbf-����� �� ����
DECLARE @regions table (
	[Guid] uniqueidentifier not null,		-- guid ������ �� ��
	[ObjectGuid] uniqueidentifier not null,	-- guid ��
	[ParentGuid] uniqueidentifier null,		-- guid �������� ��
	[TypeId] int null,						-- id ���� ��
	[TypeShortName] nvarchar(10) not null,	-- ������� ������������ ���� ��
	[Name] nvarchar(120) not null,			-- ���������� ������������ ��
	[OfficialName] nvarchar(120) null,		-- ����������� ������������ ��
	[RegionCode] nvarchar(2) not null,		-- ��� �������
	[Postcode] nvarchar(6) null,			-- �������� ������ ��
	[IsLive] bit not null,					-- ������� ������������ ��
	[IsActual] bit not null,				-- ������� ������������ ��
	[UpdateDate] date not null,				-- ���� ���������� ������ �� ��
	[PrevGuid] uniqueidentifier null)		-- guid ���������� ������ �� ��
INSERT INTO @regions (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[TypeShortName],
	[Name],
	[OfficialName],
	[RegionCode],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate],
	[PrevGuid])
EXEC(N'
SELECT
	convert(uniqueidentifier, [source].[AOID]) AS [Guid],
	convert(uniqueidentifier, [source].[AOGUID]) AS [ObjectGuid],
	convert(uniqueidentifier, [source].[PARENTGUID]) AS [ParentGuid],
	lower(dbo.Trim([source].[SHORTNAME])) AS [TypeShortName],
	dbo.Trim([source].[FORMALNAME]) AS [Name],
	CASE 
		WHEN [source].[OFFNAME] IS NULL
			OR [source].[OFFNAME] = [source].[FORMALNAME] THEN NULL
		ELSE dbo.Trim([source].[OFFNAME])
	END AS [OfficialName],
	[source].[REGIONCODE] AS [RegionCode],
	[source].[POSTALCODE] AS [Postcode],
	[source].[LIVESTATUS] AS [IsLive],
	[source].[ACTSTATUS] AS [IsActual],
	convert(date, [source].[UPDATEDATE]) AS [UpdateDate],
	convert(uniqueidentifier, [source].[PREVID]) AS [PrevGuid]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		BackgroundFetch=No;
		Deleted=No;
		Exclusive=No;
		Null=No;
		SourceType=DBF;'',
	''SELECT
		[AOID],
		[AOGUID],
		[PARENTGUID],
		[AOLEVEL],
		[SHORTNAME],
		[FORMALNAME],
		[OFFNAME],
		[REGIONCODE],
		[POSTALCODE],
		[LIVESTATUS],
		[ACTSTATUS],
		[UPDATEDATE],
		[PREVID]
	  FROM ' + @fiasAddressObjectsDbf + '
	  WHERE [AOLEVEL] = ' + @fiasRegionLevel + ''') AS [source]');

-- �������� ������������� �������
IF EXISTS(
		SELECT *
		FROM @regions
		WHERE [ParentGuid] IS NOT NULL	-- � ������� �� �.�. ��������
		OR [OfficialName] IS NOT NULL)	-- ���������� ������������ ������� ������ ��������� � �����������
	THROW 50000, N'������ �������� ��� �������, �.�. ������������� ������ �� ������ ��������.', 1;

-- ��������� �������� � ������������� �������
DECLARE @replacements table(
	[FromName] nvarchar(120) not null,
	[ToName] nvarchar(120) not null,
	[ToTypeShortName] nvarchar(10) null);
INSERT INTO @replacements
VALUES
	(N'�����-���������� ���������� ����� - ����', N'�����-���������� ���������� ����� � ����', null),
	(N'��������� ���������� -', N'���������', N'����'),
	(N'�������� ������ - ������', N'�������� ������-������', null),
	(N'��������� - �������', N'��������� � �������', null),
	(N'���� /������/', N'���� (������)', null);

UPDATE [region]
SET [Name] = [replacement].[ToName],
	[TypeShortName] = ISNULL([replacement].[ToTypeShortName], [region].[TypeShortName])
FROM @regions AS [region]
INNER JOIN @replacements AS [replacement]
	ON [region].[Name] = [replacement].[FromName];

-- Id ������� �� �� ��
DECLARE @addressRegionLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'); -- id ������ �������

-- ���������� ���� ��
UPDATE [region]
SET [TypeId] = [type].[Id]
FROM @regions AS [region]
INNER JOIN (
		SELECT [Id], [ShortName]
		FROM [dbo].[AddressObjectTypes]
		WHERE [LevelId] = @addressRegionLevelId) AS [type]
	ON [region].[TypeShortName] = [type].[ShortName];

-- ���������� ���������� � ���������� ������� � �������� � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- �����������, ����������������� � �� ����, �������� �������� ������������ �������
UPDATE [dbo].[AddressRegions]
SET [IsLive] = 0 -- �������� � 0 ������� ������������ ��,
FROM [dbo].[AddressRegions] AS [destination] -- ��� �������� �� ��, �������
INNER JOIN @regions AS [source]
	ON [destination].[ObjectGuid] = [source].[ObjectGuid] -- ����� ����� ������ �� �� �
	AND [destination].[Guid] <> [source].[Guid] -- guid ������� �� ��������� �
	AND [destination].[IsLive] = 1 AND [source].[IsLive] = 1 -- ��� �� �������� ������������ �
	AND [source].[PrevGuid] IS NOT NULL
	AND [destination].[Guid] = [source].[PrevGuid]; -- ������ �� �� � ��, �������� ���������� ������������ ����� ������

PRINT N'����������: ' + str(@@ROWCOUNT);

-- ����������, ������������ � ��, ��������
UPDATE [dbo].[AddressRegions]
SET [ObjectGuid] = [source].[ObjectGuid],
	[TypeId] = [source].[TypeId],
	[Name] = [source].[Name],
	[RegionCode] = [source].[RegionCode],
	[Postcode] = [source].[Postcode],
	[IsLive] = [source].[IsLive],
	[IsActual] = [source].[IsActual],
	[UpdateDate] = [source].[UpdateDate]
FROM [dbo].[AddressRegions] AS [destination]
INNER JOIN @regions AS [source]
	ON [destination].[Guid] = [source].[Guid];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- ����������, �������������� � ��, ��������
INSERT INTO [dbo].[AddressRegions] (
	[Guid],
	[ObjectGuid],
	[TypeId],
	[Name],
	[RegionCode],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate])
SELECT
	[Guid],
	[ObjectGuid],
	[TypeId],
	[Name],
	[RegionCode],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate]
FROM @regions AS [source]
WHERE NOT EXISTS(
	SELECT *
	FROM [dbo].[AddressRegions] AS [destination]
	WHERE [destination].[Guid] = [source].[Guid]);

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @regions);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ �������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ �������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO