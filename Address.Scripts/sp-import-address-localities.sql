-- �������� ��������� ������� ������� � �������, ��������������� �����������
-- � ��������� ������� �� �� ���� 

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressLocalities')
	DROP PROCEDURE [dbo].[ImportAddressLocalities];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressLocalities]
	@fiasAddressObjectsDbf nvarchar(max) -- ���� � dbf-����� �� ���� 
AS
BEGIN
SET NOCOUNT ON;

-- ������ �������� �������� (��) �� ���� 
DECLARE
	@fiasCityLevel int = 4,					-- ������� ������
	@fiasIntracityTerritoryLevel int = 5,	-- ������� ��������������� ����������
	@fiasLocalityLevel int = 6;				-- ������� ���������� ������

-- ������ ��������� ������� (��) �� dbf-����� �� ����
DECLARE @localities table (
	[Guid] uniqueidentifier not null,		-- guid ������ �� ��
	[ObjectGuid] uniqueidentifier not null, -- guid ��
	[ParentGuid] uniqueidentifier not null, -- guid �������� ��
	[ParentLevelId] int null,				-- ������� �������� ��
	[RegionId] int null,					-- id ������� ��
	[AreaId] int null,						-- id ������ ��
	[TypeId] int null,						-- id ���� ��
	[TypeShortName] nvarchar(10) not null,	-- ������� ������������ ���� ��
	[FormalName] nvarchar(120) not null,	-- ���������� ������������ ��
	[OfficialName] nvarchar(120) null,		-- ����������� ������������ ��
	[Postcode] nvarchar(6) null,			-- �������� ������ ��
	[IsLive] bit not null,					-- ������� ������������ ��
	[IsActual] bit not null,				-- ������� ������������ ��
	[UpdateDate] date not null,				-- ���� ���������� ������ �� ��
	[PrevGuid] uniqueidentifier null,		-- guid ���������� ������ �� ��
	[Updated] bit null);
INSERT INTO @localities (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[TypeShortName],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate],
	[PrevGuid])
EXEC(N'
SELECT
	convert(uniqueidentifier, [source].[AOID]) AS [Guid],
	convert(uniqueidentifier, [source].[AOGUID]) AS [ObjectGuid],
	convert(uniqueidentifier, [source].[PARENTGUID]) AS [ParentGuid],
	lower(dbo.Trim([source].[SHORTNAME])) AS [TypeShortName],
	dbo.Trim([source].[FORMALNAME]) AS [FormalName],
	CASE 
		WHEN [source].[OFFNAME] IS NULL
			OR [source].[OFFNAME] = [source].[FormalName] THEN NULL
		ELSE dbo.Trim([source].[OFFNAME])
	END AS [OfficialName],
	[source].[POSTALCODE] AS [Postcode],
	[source].[LIVESTATUS] AS [IsLive],
	[source].[ACTSTATUS] AS [IsActual],
	convert(date, [source].[UPDATEDATE]) AS [UpdateDate],
	convert(uniqueidentifier, [source].[PREVID]) AS [PrevGuid]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		BackgroundFetch=No;
		Deleted=No;
		Exclusive=No;
		Null=No;
		SourceType=DBF;'',
	''SELECT
		[AOID],
		[AOGUID],
		[PARENTGUID],
		[AOLEVEL],
		[SHORTNAME],
		[FORMALNAME],
		[OFFNAME],
		[POSTALCODE],
		[LIVESTATUS],
		[ACTSTATUS],
		[UPDATEDATE],
		[PREVID]
	  FROM ' + @fiasAddressObjectsDbf + '
	  WHERE [AOLEVEL] IN (' + @fiasCityLevel 
		+ ', ' + @fiasIntracityTerritoryLevel
		+ ', ' + @fiasLocalityLevel + ')'') AS [source]');

-- Id ������� �� �� ��
DECLARE
	@addressRegionLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'),				-- id ������ �������
	@addressAreaLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'),					-- id ������ ������
	@addressLocalityLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����');	-- id ������ ���������� ������

-- ���������� ���� ��
UPDATE [locality]
SET [TypeId] = [type].[Id]
FROM @localities AS [locality]
INNER JOIN (
		SELECT [Id], [ShortName]
		FROM [dbo].[AddressObjectTypes]
		WHERE [LevelId] = @addressLocalityLevelId) AS [type]
	ON [locality].[TypeShortName] = [type].[ShortName];

-- ���������� �������� �� �� �������� � ��
UPDATE [locality]
SET	[ParentLevelId] = @addressRegionLevelId,
	[ParentGuid] = [region].[Guid],
	[RegionId] = [region].[Id]
FROM @localities AS [locality]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressRegions]
		WHERE [IsLive] = 1)  AS [region] -- ������� ������� �� ����������� ��������
	ON [locality].[ParentGuid] = [region].[ObjectGuid]; 

UPDATE [locality]
SET	[ParentLevelId] = @addressRegionLevelId,
	[ParentGuid] = [region].[Guid],
	[RegionId] = [region].[Id]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [locality]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressRegions]
		WHERE [IsActual] = 1 AND [IsLive] = 0)  AS [region] -- ����� ������� ������ �� ���������� ��������
	ON [locality].[ParentGuid] = [region].[ObjectGuid];

-- ���������� �������� �� �� ������� � ��
UPDATE [locality]
SET [ParentLevelId] = @addressAreaLevelId,
	[ParentGuid] = [area].[Guid],
	[RegionId] = [area].[RegionId],
	[AreaId] = [area].[Id]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] IS NULL) AS [locality]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressAreas]
		WHERE [IsLive] = 1) AS [area] -- ������� ������� �� ����������� �������
	ON [locality].[ParentGuid] = [area].[ObjectGuid]; 

UPDATE [locality]
SET [ParentLevelId] = @addressAreaLevelId,
	[ParentGuid] = [area].[Guid],
	[RegionId] = [area].[RegionId],
	[AreaId] = [area].[Id]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [locality]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressAreas]
		WHERE [IsActual] = 1 AND [IsLive] = 0)  AS [area] -- ����� ������� ������ �� ���������� �������
	ON [locality].[ParentGuid] = [area].[ObjectGuid];

-- ���������� �������� �� �� ��������� �������, ����������� � ������������ � �� 
UPDATE [locality]
SET [ParentLevelId] = @addressLocalityLevelId,
	[ParentGuid] = [parent].[Guid],
	[RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] IS NULL) AS [locality]
INNER JOIN (
		SELECT [Guid], [ObjectGuid], [IsLive], [RegionId], [AreaId]
		FROM [dbo].[AddressLocalities]
		WHERE [IsLive] = 1 AND [Guid] NOT IN (SELECT [Guid] FROM @localities)
		UNION ALL
		SELECT [Guid], [ObjectGuid], [IsLive], [RegionId], [AreaId]
		FROM @localities
		WHERE [IsLive] = 1) AS [parent] -- ������� ������� �� ����������� ��������� �������
	ON [locality].[ParentGuid] = [parent].[ObjectGuid]; 

UPDATE [locality]
SET [ParentLevelId] = @addressLocalityLevelId,
	[ParentGuid] = [parent].[Guid],
	[RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] IS NULL AND [IsLive] = 0) AS [locality]
INNER JOIN (
		SELECT [Guid], [ObjectGuid], [IsActual], [RegionId], [AreaId]
		FROM [dbo].[AddressLocalities]
		WHERE [IsActual] = 1 AND [IsLive] = 0 AND [Guid] NOT IN (SELECT [Guid] FROM @localities)
		UNION ALL
		SELECT [Guid], [ObjectGuid], [IsActual], [RegionId], [AreaId]
		FROM @localities
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [parent] -- ����� ������� ������ �� ���������� ��������� �������
	ON [locality].[ParentGuid] = [parent].[ObjectGuid]; 

-- ���������� ������� � ������ ��� ��������� ������� � ������� �������� ���� ��������� ������
DECLARE @parents table (
	[Guid] uniqueidentifier not null,
	[ObjectGuid] uniqueidentifier not null,
	[ParentGuid] uniqueidentifier not null,
	[ParentLevelId] int not null,
	[RegionId] int null,
	[AreaId] int null);
INSERT INTO @parents
SELECT [Guid], [ObjectGuid], [ParentGuid], [ParentLevelId], [RegionId], [AreaId]
FROM [dbo].[AddressLocalities]
WHERE [Guid] NOT IN (SELECT [Guid] FROM @localities)
UNION ALL
SELECT [Guid], [ObjectGuid], [ParentGuid], [ParentLevelId], [RegionId], [AreaId]
FROM @localities;

UPDATE [locality]
SET [RegionId] = [parent].[RegionId],
	[AreaId] = [parent].[AreaId]
FROM (
	SELECT *
	FROM @localities
	WHERE [ParentLevelId] = @addressLocalityLevelId
	AND [RegionId] IS NULL) AS [locality]
INNER JOIN @parents AS [parent]
	ON [parent].[Guid] = ( -- ����������� 3 ������ �������������: ��������� ����� -> ��������������� ���������� -> �����
		SELECT CASE
			WHEN [intracityTerritory].[ParentLevelId] IN (@addressRegionLevelId, @addressAreaLevelId)
				THEN [intracityTerritory].[Guid]
			WHEN [intracityTerritory].[ParentLevelId] = @addressLocalityLevelId THEN (
				SELECT [city].[Guid]
				FROM @parents AS [city]
				WHERE [intracityTerritory].[ParentGuid] = [city].[Guid]
				AND [city].[ParentLevelId] IN (@addressRegionLevelId, @addressAreaLevelId))
			END
		FROM @parents AS [intracityTerritory]
		WHERE [locality].[ParentGuid] = [intracityTerritory].[Guid]);

-- ���������� ���������� � ���������� ������� � ��������� ������� � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- �����������, ����������������� � �� ����, �������� �������� ������������ ���������� ������
UPDATE [dbo].[AddressLocalities]
SET [IsLive] = 0 -- �������� � 0 ������� ������������ ��,
FROM [dbo].[AddressLocalities] AS [destination] -- ��� ��������� ������� �� ��, �������
INNER JOIN @localities AS [source]
	ON [destination].[ObjectGuid] = [source].[ObjectGuid] -- ����� ����� ������ �� �� �
	AND [destination].[Guid] <> [source].[Guid] -- guid ������� �� ��������� �
	AND [destination].[IsLive] = 1 AND [source].[IsLive] = 1 -- ��� �� �������� ������������ �
	AND [source].[PrevGuid] IS NOT NULL
	AND [destination].[Guid] = [source].[PrevGuid]; -- ������ �� �� � ��, �������� ���������� ������������ ����� ������

PRINT N'����������: ' + str(@@ROWCOUNT);

UPDATE @localities
SET [Updated] = CASE
		WHEN EXISTS(
			SELECT *
			FROM [dbo].[AddressLocalities] AS [destination]
			WHERE [source].[Guid] = [destination].[Guid]) THEN 1
		ELSE 0
	END
FROM @localities AS [source];

-- ����������, �������������� � ��, ��������� �������
INSERT INTO [dbo].[AddressLocalities] (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[ParentLevelId],
	[RegionId],
	[AreaId],
	[TypeId],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate])
SELECT
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[ParentLevelId],
	[RegionId],
	[AreaId],
	[TypeId],
	[FormalName],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate]
FROM @localities
WHERE [Updated] = 0;

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- ����������, ������������ � ��, ��������� �������
UPDATE [dbo].[AddressLocalities]
SET [ObjectGuid] = [source].[ObjectGuid],
	[ParentGuid] = [source].[ParentGuid],
	[ParentLevelId] = [source].[ParentLevelId],
	[RegionId] = [source].[RegionId],
	[AreaId] = [source].[AreaId],
	[TypeId] = [source].[TypeId],
	[FormalName] = [source].[FormalName],
	[OfficialName] = [source].[OfficialName],
	[Postcode] = [source].[Postcode],
	[IsLive] = [source].[IsLive],
	[IsActual] = [source].[IsActual],
	[UpdateDate] = [source].[UpdateDate]
FROM (
	SELECT *
	FROM @localities
	WHERE [Updated] = 1) AS [source]
INNER JOIN [dbo].[AddressLocalities] AS [destination]
	ON [source].[Guid] = [destination].[Guid];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @localities);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ ��������� ������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ ��������� ������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO