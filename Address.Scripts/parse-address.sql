USE [work_db]
GO

DECLARE
	@addressString nvarchar(2000) = '603006, �.�.��������, ��. �������, �.3 ��� �����ϻ',
	@limitRegionDistance float = 0.25,
	@limitAreaDistance float = 0.25,
	@limitLocalityDistance float = 0.25,
	@limitThoroughfareDistance float = 0.25,
	@maxNgramm int = 3,
	@top int = 50

-- ������ ��
DECLARE
	@regionLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'),				-- id ������ �������
	@areaLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'),					-- id ������ ������
	@localityLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����'),	-- id ������ ���������� ������
	@thoroughfareLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������');	-- id ������ ��������������

-- ����������� ������� ������
DECLARE @Tokens table (
	[Start] int not null,
	[End] int not null,
	[Number] int null,
	[Name] nvarchar(max) not null,
	[IsPostcode] bit not null,
	[IsName] bit not null,
	[IsBuilding] bit not null);
INSERT INTO @Tokens
SELECT
	[Start],
	[End],
	[Number],
	[Name],
	[IsPostcode],
	[IsName],
	[IsBuilding]
FROM dbo.AddressTokenize(@addressString, @maxNgramm);

--SELECT * FROM @Tokens;

-- ����������� �������� ��������
DECLARE @Postcodes table ([Code] int not null);
INSERT INTO @Postcodes
SELECT [token].[Number] AS [Code]
FROM @Tokens AS [token]
WHERE [token].[IsPostcode] = 1;

--SELECT * FROM @Postcodes;

-- ����������� ������� ��������
DECLARE @Buildings table (
	[Index] int not null,
	[Name] nvarchar(max) not null,
	[IsEven] bit not null,
	[TokenStart] int not null,
	[TokenEnd] int not null);
INSERT INTO @Buildings
SELECT
	[token].[Number] AS [Index],
	[token].[Name],
	CASE
		WHEN [token].[Number] % 2 = 0 THEN 1
		ELSE 0
	END AS [IsEven],
	[token].[Start] AS [TokenStart],
	[token].[End] AS [TokenEnd]
FROM @Tokens AS [token]
WHERE [token].[IsBuilding] = 1;

--SELECT * FROM @Buildings;

-- ����������� �������� ��
DELETE FROM @Tokens WHERE [IsName] = 0;

DECLARE @Names table (
	[Name] nvarchar(max) not null,
	[PhoneticName] nvarchar(max) not null,
	[TokenStart] int not null,
	[TokenEnd] int not null);
INSERT INTO @Names
SELECT *
FROM (
	SELECT
		[token].[Name],
		dbo.GetPhoneticName([token].[Name], 3) AS [PhoneticName],
		[token].[Start] AS [TokenStart],
		[token].[End] AS [TokenEnd]
	FROM @Tokens AS [token]
	WHERE LEN([token].[Name]) >= 2
	AND NOT EXISTS(
		SELECT *
		FROM @Tokens AS [source]
		WHERE LEN([source].[Name]) = 1
		AND [source].[Start] >= [token].[Start] 
		AND [source].[End] <= [token].[End])) AS [source]
WHERE [PhoneticName] > 0;

--SELECT * FROM @Names;

-- ������������ ����������
DECLARE @Addresses table (
	[RegionId] int null,
	[RegionTypeId] int null,
	[RegionName] nvarchar(max) null,
	[RegionPostcode] int null,
	[RegionIsLive] bit null,
	[RegionTokenStart] int null,
	[RegionTokenEnd] int null,
	[RegionDistance] float null,
	[RegionTypeIsDetected] bit not null,
	[RegionNameIsDetected] bit not null,
	[AreaId] int null,
	[AreaTypeId] int null,
	[AreaRegionId] int null,
	[AreaName] nvarchar(max) null,
	[AreaPostcode] int null,
	[AreaIsLive] bit null,
	[AreaTokenStart] int null,
	[AreaTokenEnd] int null,
	[AreaDistance] float null,
	[AreaTypeIsDetected] bit not null,
	[AreaNameIsDetected] bit not null,
	[LocalityId] int null,
	[LocalityTypeId] int null,
	[LocalityRegionId] int null,
	[LocalityAreaId] int null,
	[LocalityName] nvarchar(max) null,
	[LocalityPostcode] int null,
	[LocalityIsLive] bit null,
	[LocalityTokenStart] int null,
	[LocalityTokenEnd] int null,
	[LocalityDistance] float null,
	[LocalityTypeIsDetected] bit not null,
	[LocalityNameIsDetected] bit not null,
	[ThoroughfareId] int null,
	[ThoroughfareTypeId] int null,
	[ThoroughfareRegionId] int null,
	[ThoroughfareAreaId] int null,
	[ThoroughfareLocalityId] int null,
	[ThoroughfareName] nvarchar(max) null,
	[ThoroughfarePostcode] int null,
	[ThoroughfareIsLive] bit null,
	[ThoroughfareTokenStart] int null,
	[ThoroughfareTokenEnd] int null,
	[ThoroughfareDistance] float null,
	[ThoroughfareTypeIsDetected] bit not null,
	[ThoroughfareNameIsDetected] bit not null,
	[BuildingIndex] int null,
	[BuildingName] nvarchar(max) null,
	[BuildingPostcode] int null,
	[BuildingIsDetected] bit null,
	[Distance] float null
	, [a] float, [b] float, [c] float, [d] float);

-- ����� � ���������� �������� ��� ����� �� �����
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected] = 0,
	[RegionNameIsDetected] = 1,
	[AreaTypeIsDetected] = 0,
	[AreaNameIsDetected] = 0,
	[LocalityTypeIsDetected] = 0,
	[LocalityNameIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareNameIsDetected] = 0
FROM (
	SELECT
		[region].[Id] AS [RegionId],
		[region].[TypeId] AS [RegionTypeId],
		[token].[TokenStart] AS [RegionTokenStart],
		[token].[TokenEnd] AS [RegionTokenEnd],
		dbo.Levenstein([token].[Name], [region].[Name]) AS [RegionDistance]
	FROM [dbo].[AddressRegions] AS [region], @Names AS [token]
	WHERE ([region].[IsLive] = 1 OR [region].[IsActual] = 1)
	AND [region].[PhoneticName] = [token].[PhoneticName]) AS [source]
WHERE [RegionDistance] <= @limitRegionDistance;

-- ���������� �������� � ������ �� �����
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[RegionId],
	[RegionTypeId],
	CASE WHEN [Start] > [RegionTokenStart] THEN [RegionTokenStart] ELSE [Start] END AS [RegionTokenStart],
	CASE WHEN [End] > [RegionTokenEnd] THEN [End] ELSE [RegionTokenEnd] END AS [RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected] = 1,
	[RegionNameIsDetected],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected]
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @regionLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Addresses AS [region]
	ON [type].[Id] = [region].[RegionTypeId]
	AND ([token].[Start] = [region].[RegionTokenEnd] + 1 OR [token].[End] = [region].[RegionTokenStart] - 1);
/*
SELECT
	[RegionId],
	[RegionTypeId],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[Name]
FROM @Addresses
INNER JOIN [dbo].[AddressRegions]
	ON [RegionId] = [Id]
ORDER BY [RegionDistance], [RegionId], [RegionTypeIsDetected];
*/

-- ����� ������� ��� ����� �� �����
DECLARE @Areas table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Areas
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[area].[Id],
		[area].[TypeId],
		[area].[RegionId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [area].[Name]) AS [AreaDistance]
	FROM [dbo].[AddressAreas] AS [area], @Names AS [token]
	WHERE ([area].[IsLive] = 1 OR [area].[IsActual] = 1)
	AND [area].[PhoneticName] = [token].[PhoneticName]) AS [source]
WHERE [AreaDistance] <= @limitAreaDistance;

-- ����� ������� c ������ �� �����
INSERT INTO @Areas
SELECT
	[area].[Id],
	[area].[TypeId],
	[area].[RegionId],
	CASE WHEN [token].[Start] > [area].[TokenStart] THEN [area].[TokenStart] ELSE [token].[Start] END AS [TokenStart],
	CASE WHEN [token].[End] > [area].[TokenEnd] THEN [token].[End] ELSE [area].[TokenEnd] END AS [TokenEnd],
	[area].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @areaLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Areas AS [area]
	ON [type].[Id] = [area].[TypeId]
	AND ([token].[Start] = [area].[TokenEnd] + 1 OR [token].[End] = [area].[TokenStart] - 1);
/*
SELECT [area].*, [source].[Name]
FROM @Areas AS [area]
INNER JOIN [dbo].[AddressAreas] AS [source]
	ON [area].[Id] = [source].[Id]
ORDER BY [area].[Distance], [area].[Id], [area].[TypeIsDetected];
*/

-- ����� � ���������� �������� � �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaId],
	[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[address].[RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	[address].[RegionTypeIsDetected],
	[address].[RegionNameIsDetected],
	[area].[Id] AS [AreaId],
	[area].[RegionId] AS [AreaRegionId],
	[area].[TokenStart] AS [AreaTokenStart],
	[area].[TokenEnd] AS [AreaTokenEnd],
	[area].[Distance] AS [AreaDistance],
	[area].[TypeIsDetected] AS [AreaTypeIsDetected],
	[AreaNameIsDetected] = 1,
	[LocalityTypeIsDetected] = 0,
	[LocalityNameIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareNameIsDetected] = 0
FROM @Areas AS [area]
INNER JOIN @Addresses AS [address]
	ON [area].[RegionId] = [address].[RegionId]
	AND ([area].[TokenStart] > [address].[RegionTokenEnd] OR [area].[TokenEnd] < [address].[RegionTokenStart]);

-- ���������� �������
INSERT INTO @Addresses (
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaId],
	[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[RegionTypeIsDetected] = 0,
	[RegionNameIsDetected] = 0,
	[area].[Id] As [AreaId],
	[area].[RegionId] AS [AreaRegionId],
	[area].[TokenStart] AS [AreaTokenStart],
	[area].[TokenEnd] AS [AreaTokenEnd],
	[area].[Distance] As [AreaDistance],
	[area].[TypeIsDetected] AS [AreaTypeIsDetected],
	[AreaNameIsDetected] = 1,
	[LocalityTypeIsDetected] = 0,
	[LocalityNameIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareNameIsDetected] = 0
FROM @Areas AS [area];

-- ����� ��������� ������� ��� ����� �� �����
DECLARE @Localities table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[AreaId] int null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Localities
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	(SELECT
		[locality].[Id],
		[locality].[TypeId],
		[locality].[RegionId],
		[locality].[AreaId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [locality].[FormalName]) AS [Distance]
		FROM [dbo].[AddressLocalities] AS [locality], @Names AS [token]
		WHERE ([locality].[IsLive] = 1 OR [locality].[IsActual] = 1) AND [locality].[PhoneticFormalName] = [token].[PhoneticName])
	UNION ALL
	(SELECT
		[locality].[Id],
		[locality].[TypeId],
		[locality].[RegionId],
		[locality].[AreaId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [locality].[OfficialName]) AS [Distance]
		FROM [dbo].[AddressLocalities] AS [locality], @Names AS [token]
		WHERE ([locality].[IsLive] = 1 OR [locality].[IsActual] = 1) AND [locality].[PhoneticOfficialName] = [token].[PhoneticName])) AS [source]
WHERE [Distance] <= @limitLocalityDistance;

-- ����� ��������� ������� � ������ �� �����
INSERT INTO @Localities
SELECT
	[locality].[Id],
	[locality].[TypeId],
	[locality].[RegionId],
	[locality].[AreaId],
	CASE WHEN [token].[Start] < [locality].[TokenStart] THEN [token].[Start] ELSE [locality].[TokenStart] END AS [TokenStart],
	CASE WHEN [token].[End] < [locality].[TokenEnd] THEN [locality].[TokenEnd] ELSE [token].[End] END AS [TokenEnd],
	[locality].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @localityLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Localities AS [locality]
	ON [type].[Id] = [locality].[TypeId]
	AND ([token].[End] = [locality].[TokenStart] - 1 OR [token].[Start] = [locality].[TokenEnd] + 1);

-- ����� � ���������� ��������, ������� � ��������� �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaId],
	[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityId],
	[LocalityRegionId],
	[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[address].[RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	[address].[RegionTypeIsDetected],
	[address].[RegionNameIsDetected],
	[address].[AreaId],
	[address].[AreaRegionId],
	[address].[AreaTokenStart],
	[address].[AreaTokenEnd],
	[address].[AreaDistance],
	[address].[AreaTypeIsDetected],
	[address].[AreaNameIsDetected],
	[locality].[Id],
	[locality].[RegionId],
	[locality].[AreaId],
	[locality].[TokenStart],
	[locality].[TokenEnd],
	[locality].[Distance],
	[locality].[TypeIsDetected],
	[LocalityNameIsDetected] = 1,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareNameIsDetected] = 0
FROM @Addresses AS [address]
INNER JOIN @Localities AS [locality]
		ON ([address].[RegionId] IS NULL
			AND [address].[AreaId] = [locality].[AreaId]
			AND ([address].[AreaTokenStart] > [locality].[TokenEnd] OR [address].[AreaTokenEnd] < [locality].[TokenStart]))
		OR (([address].[RegionId] = [locality].[RegionId]
				AND ([address].[RegionTokenStart] > [locality].[TokenEnd] OR [address].[RegionTokenEnd] < [locality].[TokenStart]))
			AND ([address].[AreaId] IS NULL
				OR ([address].[AreaId] = [locality].[AreaId]
					AND ([address].[AreaTokenStart] > [locality].[TokenEnd] OR [address].[AreaTokenEnd] < [locality].[TokenStart]))));

-- ���������� ��������� �������
INSERT INTO @Addresses (
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityId],
	[LocalityRegionId],
	[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	[RegionTypeIsDetected] = 0,
	[RegionNameIsDetected] = 0,
	[AreaTypeIsDetected] = 0,
	[AreaNameIsDetected] = 0,
	[locality].[Id] As [LocalityId],
	[locality].[RegionId] AS [LocalityRegionId],
	[locality].[AreaId] AS [LocalityAreaId],
	[locality].[TokenStart] AS [LocalityTokenStart],
	[locality].[TokenEnd] AS [LocalityTokenEnd],
	[locality].[Distance] As [LocalityDistance],
	[locality].[TypeIsDetected] AS [LocalityTypeIsDetected],
	[LocalityNameIsDetected] = 1,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareNameIsDetected] = 0
FROM @Localities AS [locality];

-- ����� �������������� ��� ����� �� �����
DECLARE @Thoroughfares table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[AreaId] int null, 
	[LocalityId] int null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Thoroughfares
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	(SELECT
		[thoroughfare].[Id],
		[thoroughfare].[TypeId],
		[thoroughfare].[RegionId],
		[thoroughfare].[AreaId],
		[thoroughfare].[LocalityId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [thoroughfare].[FormalName]) AS [Distance]
		FROM [dbo].[AddressThoroughfares] AS [thoroughfare], @Names AS [token]
		WHERE ([thoroughfare].[IsLive] = 1 OR [thoroughfare].[IsActual] = 1) AND [thoroughfare].[PhoneticFormalName] = [token].[PhoneticName])
	UNION ALL
	(SELECT
		[thoroughfare].[Id],
		[thoroughfare].[TypeId],
		[thoroughfare].[RegionId],
		[thoroughfare].[AreaId],
		[thoroughfare].[LocalityId],
		[token].[TokenStart] AS [TokenStart],
		[token].[TokenEnd] AS [TokenEnd],
		dbo.Levenstein([token].[Name], [thoroughfare].[OfficialName]) AS [Distance]
		FROM [dbo].[AddressThoroughfares] AS [thoroughfare], @Names AS [token]
		WHERE ([thoroughfare].[IsLive] = 1 OR [thoroughfare].[IsActual] = 1) AND [thoroughfare].[PhoneticOfficialName] = [token].[PhoneticName])) AS [source]
WHERE [Distance] <= @limitThoroughfareDistance;

-- ����� �������������� � ������ �� �����
INSERT INTO @Thoroughfares
SELECT
	[thoroughfare].[Id],
	[thoroughfare].[TypeId],
	[thoroughfare].[RegionId],
	[thoroughfare].[AreaId],
	[thoroughfare].[LocalityId],
	CASE WHEN [token].[Start] < [thoroughfare].[TokenStart] THEN [token].[Start] ELSE [thoroughfare].[TokenStart] END AS [TokenStart],
	CASE WHEN [token].[End] < [thoroughfare].[TokenEnd] THEN [thoroughfare].[TokenEnd] ELSE [token].[End] END AS [TokenEnd],
	[thoroughfare].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @thoroughfareLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Thoroughfares AS [thoroughfare]
	ON [type].[Id] = [thoroughfare].[TypeId]
	AND ([token].[End] = [thoroughfare].[TokenStart] - 1 OR [token].[Start] = [thoroughfare].[TokenEnd] + 1);

-- ����� � ���������� ��������, �������, ��������� ������� � ��������������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionNameIsDetected],
	[AreaId],
	[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaNameIsDetected],
	[LocalityId],
	[LocalityRegionId],
	[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityNameIsDetected],
	[ThoroughfareId],
	[ThoroughfareRegionId],
	[ThoroughfareAreaId],
	[ThoroughfareLocalityId],
	[ThoroughfareTokenStart],
	[ThoroughfareTokenEnd],
	[ThoroughfareDistance],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareNameIsDetected])
SELECT
	ISNULL([address].[RegionId], ISNULL([address].[AreaRegionId], ISNULL([address].[LocalityRegionId], [thoroughfare].[RegionId]))) AS [RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	ISNULL([address].[RegionTypeIsDetected], 0) AS [RegionTypeIsDetected],
	ISNULL([address].[RegionNameIsDetected], 0) AS [RegionNameIsDetected],
	ISNULL([address].[AreaId], ISNULL([address].[LocalityAreaId], [thoroughfare].[AreaId])) AS [AreaId],
	[address].[AreaRegionId],
	[address].[AreaTokenStart],
	[address].[AreaTokenEnd],
	[address].[AreaDistance],
	ISNULL([address].[AreaTypeIsDetected], 0) AS [AreaTypeIsDetected],
	ISNULL([address].[AreaNameIsDetected], 0) AS [AreaNameIsDetected],
	ISNULL([address].[LocalityId], [thoroughfare].[LocalityId]) AS [LocalityId],
	[address].[LocalityRegionId],
	[address].[LocalityAreaId],
	[address].[LocalityTokenStart],
	[address].[LocalityTokenEnd],
	[address].[LocalityDistance],
	ISNULL([address].[LocalityTypeIsDetected], 0) AS [LocalityTypeIsDetected],
	ISNULL([address].[LocalityNameIsDetected], 0) AS [LocalityNameIsDetected],
	[thoroughfare].[Id] AS [ThoroughfareId],
	[thoroughfare].[RegionId] AS [ThoroughfareRegionId],
	[thoroughfare].[AreaId] AS [ThoroughfareAreaId],
	[thoroughfare].[LocalityId] AS [ThoroughfareLocalityId],
	[thoroughfare].[TokenStart] AS [ThoroughfareTokenStart],
	[thoroughfare].[TokenEnd] AS [ThoroughfareTokenEnd],
	[thoroughfare].[Distance] AS [ThoroughfareDistance],
	ISNULL([thoroughfare].[TypeIsDetected], 0) AS [ThoroughfareTypeIsDetected],
	CASE WHEN [thoroughfare].[Id] IS NULL THEN 0 ELSE 1 END AS [ThoroughfareNameIsDetected]
FROM @Addresses AS [address]
FULL JOIN @Thoroughfares AS [thoroughfare]
	ON ([address].[RegionId] IS NULL
		AND (([address].[AreaId] IS NULL
				AND [address].[LocalityId] = [thoroughfare].[LocalityId]
				AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))
			OR (([address].[AreaId] = [thoroughfare].[AreaId]
					AND ([address].[AreaTokenStart] > [thoroughfare].[TokenEnd] OR [address].[AreaTokenEnd] < [thoroughfare].[TokenStart]))
				AND ([address].[LocalityId] IS NULL
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))))
	OR ([address].[RegionId] = [thoroughfare].[RegionId]
		AND ([address].[RegionTokenStart] > [thoroughfare].[TokenEnd] OR [address].[RegionTokenEnd] < [thoroughfare].[TokenStart])
		AND (([address].[AreaId] IS NULL
				AND ([address].[LocalityId] IS NULL
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))
			OR ([address].[AreaId] = [thoroughfare].[AreaId]
				AND ([address].[AreaTokenStart] > [thoroughfare].[TokenEnd] OR [address].[AreaTokenEnd] < [thoroughfare].[TokenStart])
				AND ([address].[LocalityId] IS NULL
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))));

-- ����� � ���������� ������� ��������
UPDATE [address]
SET [BuildingIndex] = [building].[Index],
	[BuildingName] = [building].[Name]
FROM (
	SELECT *,
		CASE WHEN [ThoroughfareNameIsDetected] = 1
			THEN [ThoroughfareTokenEnd]
			ELSE [LocalityTokenEnd]
		END + 1 AS [TokenEnd]
	FROM @Addresses 
	WHERE [ThoroughfareNameIsDetected] = 1 OR [LocalityNameIsDetected] = 1) AS [address]
CROSS APPLY (
	SELECT TOP(1) *
	FROM (
		SELECT
			[building].*,
			([building].[TokenStart] - [address].[TokenEnd]) AS [TokenDistance]
		FROM @Buildings AS [building]
		WHERE [building].[TokenStart] >= [address].[TokenEnd]) AS [building]
	WHERE [building].[TokenDistance] <= 1
	ORDER BY [TokenDistance]) AS [building]
WHERE [building].[TokenDistance] = 0
OR (([address].[RegionTokenEnd] IS NULL OR [address].[RegionTokenStart] <> [address].[TokenEnd])
	AND ([address].[AreaTokenEnd] IS NULL OR [address].[AreaTokenStart] <> [address].[TokenEnd])
	AND ([ThoroughfareNameIsDetected] = 0 OR [address].[LocalityTokenStart] <> [address].[TokenEnd]));

UPDATE @Addresses
SET [BuildingIsDetected] = 1,
	[BuildingPostcode] = [interval].[Postcode]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressBuildingsIntervals] AS [interval]
	ON [address].[LocalityNameIsDetected] = 1
	AND [address].[BuildingIndex] IS NOT NULL
	AND [interval].[ParentLevelId] = @localityLevelId
	AND [interval].[ParentId] = [address].[LocalityId]
	AND [address].[BuildingIndex] >= [interval].[StartIndex]
	AND [address].[BuildingIndex] <= [interval].[EndIndex];

UPDATE @Addresses
SET [BuildingIsDetected] = 1,
	[BuildingPostcode] = [interval].[Postcode]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressBuildingsIntervals] AS [interval]
	ON [address].[ThoroughfareNameIsDetected] = 1
	AND [address].[BuildingIndex] IS NOT NULL
	AND [interval].[ParentLevelId] = @thoroughfareLevelId
	AND [interval].[ParentId] = [address].[ThoroughfareId]
	AND [address].[BuildingIndex] >= [interval].[StartIndex]
	AND [address].[BuildingIndex] <= [interval].[EndIndex];

--
DECLARE @a float, @b float, @c float, @d float; 
UPDATE @Addresses
SET 
	@a = ISNULL([RegionDistance] * CASE WHEN [RegionTypeIsDetected] = 0 THEN 1 ELSE 0.85 END, @limitRegionDistance),
	@b = CASE WHEN [AreaNameIsDetected] = 0
			THEN CASE WHEN [LocalityAreaId] IS NULL AND [LocalityNameIsDetected] = 1
					OR [ThoroughfareAreaId] IS NULL AND [ThoroughfareNameIsDetected] = 1
				THEN 0
				ELSE @limitAreaDistance END
			ELSE CASE WHEN ([LocalityAreaId] IS NULL AND [ThoroughfareAreaId] IS NULL)
					OR [LocalityAreaId] IS NOT NULL
					OR [ThoroughfareAreaId] IS NOT NULL
				THEN [AreaDistance] * CASE WHEN [AreaTypeIsDetected] = 0 THEN 1 ELSE 0.85 END
				ELSE @limitAreaDistance END
			END,
	@c = CASE WHEN [LocalityNameIsDetected] = 0
			THEN CASE WHEN [ThoroughfareLocalityId] IS NULL AND [ThoroughfareNameIsDetected] = 1
				THEN 0
				ELSE @limitLocalityDistance END
			ELSE CASE WHEN [ThoroughfareNameIsDetected] = 0 OR [ThoroughfareLocalityId] IS NOT NULL
				THEN [LocalityDistance] * CASE WHEN [LocalityTypeIsDetected] = 0 THEN 1 ELSE 0.85 END
				ELSE @limitLocalityDistance END
			END,
	@d = CASE WHEN [ThoroughfareNameIsDetected] = 0
			THEN @limitThoroughfareDistance
			ELSE [ThoroughfareDistance] * CASE WHEN [ThoroughfareTypeIsDetected] = 0 THEN 1 ELSE 0.85 END
			END,
	[Distance] = (@a + @b + @c + @d)
	, [a] = @a, [b] = @b, [c] = @c, [d] = @d
FROM @Addresses;

-- ����� � ���������� ����������� ������ ������
UPDATE @Addresses
SET	[RegionTypeId] = [type].[Id],
	[RegionName] = [region].[Name],
	[RegionPostcode] = [region].[Postcode],
	[RegionIsLive] = [region].[IsLive]
FROM @Addresses AS [address], 
	 (SELECT *
	  FROM [dbo].[AddressRegions]
	  WHERE [IsLive] = 1 OR [IsActual] = 1) AS [region],
	 (SELECT *
	  FROM [dbo].[AddressObjectTypes]
	  WHERE [LevelId] = @regionLevelId) AS [type] 
WHERE [address].[RegionId] = [region].[Id]
AND [region].[TypeId] = [type].[Id];

UPDATE @Addresses
SET [AreaTypeId] = [type].[Id],
	[AreaName] = [area].[Name],
	[AreaPostcode] = [area].[Postcode],
	[AreaIsLive] = [area].[IsLive]
FROM @Addresses AS [address], 
	(SELECT *
	 FROM [dbo].[AddressAreas]
	 WHERE [IsLive] = 1 OR [IsActual] = 1) AS [area],
	(SELECT *
	 FROM [dbo].[AddressObjectTypes]
	 WHERE [LevelId] = @areaLevelId) AS [type]
WHERE [address].[AreaId] IS NOT NULL
AND [address].[AreaId] = [area].[Id]
AND [area].[TypeId] = [type].[Id];

UPDATE @Addresses
SET [LocalityTypeId] = [type].[Id],
	[LocalityName] = ISNULL([locality].[OfficialName], [locality].[FormalName]),
	[LocalityPostcode] = [locality].[Postcode],
	[LocalityIsLive] = [locality].[IsLive]
FROM @Addresses AS [address],
	(SELECT *
	 FROM [dbo].[AddressLocalities]
	 WHERE [IsLive] = 1 OR [IsActual] = 1) AS [locality],
	(SELECT *
	 FROM [dbo].[AddressObjectTypes]
	 WHERE [LevelId] = @localityLevelId) AS [type]
WHERE [address].[LocalityId] IS NOT NULL
AND [address].[LocalityId] = [locality].[Id]
AND [locality].[TypeId] = [type].[Id];

UPDATE @Addresses
SET [ThoroughfareTypeId] = [type].[Id],
	[ThoroughfareName] = ISNULL([thoroughfare].[OfficialName], [thoroughfare].[FormalName]),
	[ThoroughfarePostcode] = [thoroughfare].[Postcode],
	[ThoroughfareIsLive] = [thoroughfare].[IsLive]
FROM @Addresses AS [address], 
	(SELECT *
	 FROM [dbo].[AddressThoroughfares]
	 WHERE [IsLive] = 1 OR [IsActual] = 1) AS [thoroughfare],
	(SELECT *
	 FROM [dbo].[AddressObjectTypes]
	 WHERE [LevelId] = @thoroughfareLevelId) AS [type]
WHERE [address].[ThoroughfareId] IS NOT NULL
AND [address].[ThoroughfareId] = [thoroughfare].[Id]
AND [thoroughfare].[TypeId] = [type].[Id];

-- �������� ����������� �������
DELETE FROM [address]
FROM @Addresses AS [address]
INNER JOIN (
		SELECT [RegionId], [AreaId], [LocalityId], [ThoroughfareId], MIN([Distance]) AS [Distance]
		FROM @Addresses
		GROUP BY [RegionId], [AreaId], [LocalityId], [ThoroughfareId]) AS [source]
	ON (([address].[RegionId] IS NULL AND [source].[RegionId] IS NULL) OR [address].[RegionId] = [source].[RegionId])
	AND (([address].[AreaId] IS NULL AND [source].[AreaId] IS NULL) OR [address].[AreaId] = [source].[AreaId])
	AND (([address].[LocalityId] IS NULL AND [source].[LocalityId] IS NULL) OR [address].[LocalityId] = [source].[LocalityId])
	AND (([address].[ThoroughfareId] IS NULL AND [source].[ThoroughfareId] IS NULL) OR [address].[ThoroughfareId] = [source].[ThoroughfareId])
	AND [address].[Distance] > [source].[Distance];
	
DELETE FROM [address]
FROM @Addresses AS [address]
INNER JOIN (
		SELECT [RegionId], [AreaId], [LocalityId], [ThoroughfareId],
			MAX(CASE WHEN [RegionTypeIsDetected] = 0 THEN 0 ELSE 1 END
				+ CASE WHEN [AreaTypeIsDetected] = 0 THEN 0 ELSE 1 END
				+ CASE WHEN [LocalityTypeIsDetected] = 0 THEN 0 ELSE 1 END
				+ CASE WHEN [ThoroughfareTypeIsDetected] = 0 THEN 0 ELSE 1 END
				+ CASE WHEN [RegionIsLive] = 1 THEN 1 ELSE 0 END
				+ CASE WHEN [AreaIsLive] = 1 THEN 1 ELSE 0 END
				+ CASE WHEN [LocalityIsLive] = 1 THEN 1 ELSE 0 END
				+ CASE WHEN [ThoroughfareIsLive] = 1 THEN 1 ELSE 0 END
				+ CASE WHEN [BuildingIndex] IS NULL THEN 0 ELSE 1 END) AS [Count]
		FROM @Addresses
		GROUP BY [RegionId], [AreaId], [LocalityId], [ThoroughfareId]) AS [source]
	ON (([address].[RegionId] IS NULL AND [source].[RegionId] IS NULL) OR [address].[RegionId] = [source].[RegionId])
	AND (([address].[AreaId] IS NULL AND [source].[AreaId] IS NULL) OR [address].[AreaId] = [source].[AreaId])
	AND (([address].[LocalityId] IS NULL AND [source].[LocalityId] IS NULL) OR [address].[LocalityId] = [source].[LocalityId])
	AND (([address].[ThoroughfareId] IS NULL AND [source].[ThoroughfareId] IS NULL) OR [address].[ThoroughfareId] = [source].[ThoroughfareId])
	AND (CASE WHEN [RegionTypeIsDetected] = 0 THEN 0 ELSE 1 END
		+ CASE WHEN [AreaTypeIsDetected] = 0 THEN 0 ELSE 1 END
		+ CASE WHEN [LocalityTypeIsDetected] = 0 THEN 0 ELSE 1 END
		+ CASE WHEN [ThoroughfareTypeIsDetected] = 0 THEN 0 ELSE 1 END
		+ CASE WHEN [RegionIsLive] = 1 THEN 1 ELSE 0 END
		+ CASE WHEN [AreaIsLive] = 1 THEN 1 ELSE 0 END
		+ CASE WHEN [LocalityIsLive] = 1 THEN 1 ELSE 0 END
		+ CASE WHEN [ThoroughfareIsLive] = 1 THEN 1 ELSE 0 END
		+ CASE WHEN [BuildingIndex] IS NULL THEN 0 ELSE 1 END) < [source].[Count]; 

-- ������������ �������������� ����������
DECLARE
	@sumLimitDistance float = @limitRegionDistance + @limitAreaDistance + @limitLocalityDistance + @limitThoroughfareDistance;
/*
DECLARE @Results table (
	[Postcode] int null,
	[PostcodeIsDetected] bit not null,
	[RegionId] int null,
	[RegionTypeId] int null,
	[RegionName] nvarchar(max) null,
	[RegionIsLive] bit null, 
	[RegionTypeIsDetected] bit null,
	[RegionIsDetected] bit not null,
	[AreaId] int null,
	[AreaTypeId] int null,
	[AreaName] nvarchar(max) null,
	[AreaIsLive] bit null,
	[AreaTypeIsDetected] bit null,
	[AreaIsDetected] bit not null,
	[LocalityId] int null,
	[LocalityTypeId] int null,
	[LocalityName] nvarchar(max) null,
	[LocalityIsLive] bit null,
	[LocalityTypeIsDetected] bit not null,
	[LocalityIsDetected] bit not null,
	[ThoroughfareId] int null,
	[ThoroughfareTypeId] int null,
	[ThoroughfareName] nvarchar(max) null,
	[ThoroughfareIsLive] bit null,
	[ThoroughfareTypeIsDetected] bit null,
	[ThoroughfareIsDetected] bit not null,
	[Building] nvarchar(max) null,
	[BuildingIsDetected] bit not null,
	[Relevance] float not null);
INSERT INTO @Results
*/
SELECT TOP(@top) *
FROM (
	SELECT DISTINCT
		ISNULL([postcode].[Code], ISNULL([address].[BuildingPostcode], ISNULL([address].[ThoroughfarePostcode], ISNULL([address].[LocalityPostcode], ISNULL([address].[AreaPostcode], [address].[RegionPostcode]))))) AS [Postcode],
		CASE WHEN [postcode].[Code] IS NULL THEN 0 ELSE 1 END AS [PostcodeIsDetected],
		[address].[RegionId],
		[address].[RegionTypeId],
		[address].[RegionName],
		[address].[RegionIsLive],
		[address].[RegionTokenStart],
		[address].[RegionTokenEnd],
		[address].[RegionTypeIsDetected],
		[address].[RegionNameIsDetected] AS [RegionIsDetected],
		[address].[AreaId],
		[address].[AreaTypeId],
		[address].[AreaName],
		[address].[AreaIsLive],
		[address].[AreaTokenStart],
		[address].[AreaTokenEnd],
		[address].[AreaTypeIsDetected],
		[address].[AreaNameIsDetected] AS [AreaIsDetected],
		[address].[LocalityId],
		[address].[LocalityTypeId],
		[address].[LocalityName],
		[address].[LocalityIsLive],
		[address].[LocalityTokenStart],
		[address].[LocalityTokenEnd],
		[address].[LocalityTypeIsDetected],
		[address].[LocalityNameIsDetected] AS [LocalityIsDetected],
		[address].[ThoroughfareId],
		[address].[ThoroughfareTypeId],
		[address].[ThoroughfareName],
		[address].[ThoroughfareIsLive],
		[address].[ThoroughfareTokenStart],
		[address].[ThoroughfareTokenEnd],
		[address].[ThoroughfareTypeIsDetected],
		[address].[ThoroughfareNameIsDetected] AS [ThoroughfareIsDetected],
		[address].[BuildingName] AS [Building],
		ISNULL([address].[BuildingIsDetected], 0) AS [BuildingIsDetected],
		(1 - ([address].[Distance] - CASE WHEN [postcode].[Code] IS NULL
				THEN 0
				ELSE CASE [postcode].[Code]
					WHEN [BuildingPostcode] THEN CASE WHEN [address].[Distance] > @limitThoroughfareDistance THEN @limitThoroughfareDistance ELSE [address].[Distance] END
					WHEN [ThoroughfarePostcode] THEN CASE WHEN [address].[Distance] > @limitThoroughfareDistance THEN @limitThoroughfareDistance ELSE [address].[Distance] END
					WHEN [LocalityPostcode] THEN CASE WHEN [address].[Distance] > @limitLocalityDistance THEN @limitLocalityDistance ELSE [address].[Distance] END
					WHEN [AreaPostcode] THEN CASE WHEN [address].[Distance] > @limitAreaDistance THEN @limitAreaDistance ELSE [address].[Distance] END
					WHEN [RegionPostcode] THEN CASE WHEN [address].[Distance] > @limitRegionDistance THEN @limitRegionDistance ELSE [address].[Distance] END
				END
			END) / @sumLimitDistance) AS [Relevance]
		, [a], [b], [c], [d]
	FROM @Addresses AS [address]
	LEFT JOIN @Postcodes AS [postcode]
		ON [postcode].[Code] IN (      
			[address].[BuildingPostcode],
			[address].[ThoroughfarePostcode],
			[address].[LocalityPostcode],
			[address].[AreaPostcode],
			[address].[RegionPostcode])) AS [result]
ORDER BY [Relevance] DESC,
	(CASE WHEN [PostcodeIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [RegionIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [AreaIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [LocalityIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [ThoroughfareIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [BuildingIsDetected] = 0 THEN 0 ELSE 1 END) DESC,
	(CASE WHEN [RegionTypeIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [AreaTypeIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [LocalityTypeIsDetected] = 0 THEN 0 ELSE 1 END +
	 CASE WHEN [ThoroughfareTypeIsDetected] = 0 THEN 0 ELSE 1 END) DESC,
	(ISNULL([RegionTokenEnd] - [RegionTokenStart], 0)
	+ ISNULL([AreaTokenEnd] - [AreaTokenStart], 0)
	+ ISNULL([LocalityTokenEnd] - [LocalityTokenStart], 0)
	+ ISNULL([ThoroughfareTokenEnd] - [ThoroughfareTokenStart], 0)) DESC,
	[ThoroughfareIsLive] DESC,
	[LocalityIsLive] DESC,
	[AreaIsLive] DESC,
	[RegionIsLive] DESC,
	[Postcode] DESC;