-- ������� �� ���������� � ���������� ������� � ������� ��������

USE [work_db]
GO

-- �������� ������������� ��������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'TR' AND name = N'InsertUpdate_AddressRegions')
	DROP TRIGGER [dbo].[InsertUpdate_AddressRegions];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� ��������
CREATE TRIGGER [dbo].[InsertUpdate_AddressRegions]
ON [dbo].[AddressRegions]
AFTER INSERT, UPDATE 
AS
BEGIN
SET NOCOUNT ON;

-- ��� ����������� �������, ���� ���������� ������, �� �.�. ������ �������
IF EXISTS(
	SELECT *
	FROM inserted AS [in]
	INNER JOIN deleted AS [out]
		ON [in].[Guid] = [out].[Guid]
		AND [in].[UpdateDate] < [out].[UpdateDate])
BEGIN
	RAISERROR(N'��� ����������� �������, ���� ���������� ������, �� ����� ���� ������ �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ������ ������ ����� ���, ���������� ��� �������
IF EXISTS(
	SELECT [TypeId]
	FROM inserted
	GROUP BY [TypeId]
	HAVING [TypeId] NOT IN (
		SELECT [type].[Id]
		FROM (
			SELECT [Id]
			FROM [dbo].[AddressObjectLevels]
			WHERE [Name] = N'������') AS [level]
		INNER JOIN [dbo].[AddressObjectTypes] AS [type]
			ON [level].[Id] = [type].[LevelId]))
BEGIN
	RAISERROR(N'������ ������ ����� ���, ���������� ��� �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- �.�. �� ����� ������ �������������� ����������� �������, ����� ������� � �������
IF EXISTS(
	SELECT [ObjectGuid] 
	FROM [dbo].[AddressRegions]
	WHERE [IsLive] = 0 AND [IsActual] = 1
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ �������������� ����������� �������, � ������� � �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ����������� �������
DECLARE @regions table (
	[ObjectGuid] uniqueidentifier not null,
	[IsLive] bit not null,
	[Name] nvarchar(max) not null,
	[TypeId] int not null,
	[RegionCode] tinyint not null,
	[Postcode] int null
);
INSERT INTO @regions
SELECT
	[ObjectGuid],
	[IsLive],
	[Name],
	[TypeId],
	[RegionCode],
	[Postcode]
FROM [dbo].[AddressRegions]
WHERE [IsLive] = 1;

-- �.�. �� ����� ������ ������������ �������, ����� ������� � �������
IF EXISTS(
	SELECT [ObjectGuid] 
	FROM @regions
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ ������������ �������, � ������� � �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ��� �������, �� ����������� ��������, ��� ������������ �.�. ����������
IF EXISTS(
	SELECT [Name] 
	FROM @regions
	GROUP BY [Name], [TypeId]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'��� �������, �� ����������� ��������, ��� ������������ ������ ���� ����������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ��� �������, �� ����������� ��������, ��� ��� �.�. ����������
IF EXISTS(
	SELECT [RegionCode] 
	FROM @regions
	GROUP BY [RegionCode]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'��� �������, �� ����������� ��������, ��� ��� ������ ���� ����������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ��� �������, �� ����������� ��������, ��� �������� ������ �.�. ����������
IF EXISTS(
	SELECT [Postcode] 
	FROM @regions
	WHERE [Postcode] IS NOT NULL
	GROUP BY [Postcode]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'��� �������, �� ����������� ��������, ��� �������� ������ ������ ���� ����������.', 10, 1);
	ROLLBACK;
	RETURN
END

RETURN
END
GO