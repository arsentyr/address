USE [work_db]
GO
--/*
IF EXISTS(SELECT * FROM [sys].[objects] WHERE [type] = N'TF' AND [name] = N'ParseAddress')
	DROP FUNCTION [dbo].[ParseAddress];
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ParseAddress] (
--*/
--DECLARE
	@addressString nvarchar(2000) --= '- ��������� �� ������ ���������� � �.���������, ����������� �������, �.������������, ������������ �������; - �������� �� ������ ���������� �.������, ����������� �������.'
	, @limitRegionDistance float --= 0.3
	, @limitAreaDistance float --= 0.3
	, @limitLocalityDistance float --= 0.3
	, @limitThoroughfareDistance float --= 0.3
	, @maxNgramm int --= 3
	, @top int --= 20
--/*
) RETURNS @Results table (
	[Postcode] int null,
	[PostcodeIsDetected] bit not null,
	[RegionId] int null,
	[RegionName] nvarchar(max) null,
	[RegionTypeIsDetected] bit null,
	[RegionIsDetected] bit not null,
	[AreaId] int null,
	[AreaName] nvarchar(max) null,
	[AreaTypeIsDetected] bit null,
	[AreaIsDetected] bit not null,
	[LocalityId] int null,
	[LocalityName] nvarchar(max) null,
	[LocalityTypeIsDetected] bit not null,
	[LocalityIsDetected] bit not null,
	[ThoroughfareId] int null,
	[ThoroughfareName] nvarchar(max) null,
	[ThoroughfareTypeIsDetected] bit null,
	[ThoroughfareIsDetected] bit not null,
	[Building] nvarchar(max) null,
	[BuildingIsDetected] bit not null,
	[Relevance] float not null)
AS
--*/
BEGIN
-- �������� ������� ����������
IF (@limitRegionDistance < 0 OR @limitRegionDistance > 1
	OR @limitAreaDistance < 0 OR @limitAreaDistance > 1
	OR @limitLocalityDistance < 0 OR @limitLocalityDistance > 1
	OR @limitThoroughfareDistance < 0 OR @limitThoroughfareDistance > 1
	OR @maxNgramm <= 0
	OR @top <= 0)
	RETURN

-- ������ ��
DECLARE
	@regionLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'������'),				-- id ������ �������
	@areaLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'),					-- id ������ ������
	@localityLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����'),	-- id ������ ���������� ������
	@thoroughfareLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������');	-- id ������ ��������������

-- ����������� ������� ������
DECLARE @Tokens table (
	[Start] int not null,
	[End] int not null,
	[Number] int null,
	[Name] nvarchar(max) not null,
	[IsPostcode] bit not null,
	[IsName] bit not null,
	[IsBuilding] bit not null);
INSERT INTO @Tokens
SELECT
	[Start],
	[End],
	[Number],
	[Name],
	[IsPostcode],
	[IsName],
	[IsBuilding]
FROM dbo.AddressTokenize(@addressString, @maxNgramm);

-- ����������� �������� ��������
DECLARE @Postcodes table (
	[Code] int not null,
	[TokenStart] int not null,
	[TokenEnd] int not null);
INSERT INTO @Postcodes
SELECT
	[token].[Number] AS [Code],
	[token].[Start] AS [TokenStart],
	[token].[End] AS [TokenEnd]
FROM @Tokens AS [token]
WHERE [token].[IsPostcode] = 1;

-- ����������� ������� ��������
DECLARE @Buildings table (
	[Index] int not null,
	[Name] nvarchar(max) not null,
	[IsEven] bit not null,
	[TokenStart] int not null,
	[TokenEnd] int not null);
INSERT INTO @Buildings
SELECT
	[token].[Number] AS [Index],
	[token].[Name],
	CASE
		WHEN [token].[Number] % 2 = 0 THEN 1
		ELSE 0
	END AS [IsEven],
	[token].[Start] AS [TokenStart],
	[token].[End] AS [TokenEnd]
FROM @Tokens AS [token]
WHERE [token].[IsBuilding] = 1;

-- ����������� �������� ��
DELETE FROM @Tokens WHERE [IsName] = 0;

DECLARE @Names table (
	[Name] nvarchar(max) not null,
	[PhoneticName] nvarchar(max) not null,
	[TokenStart] int not null,
	[TokenEnd] int not null);
INSERT INTO @Names
SELECT *
FROM (
	SELECT
		[token].[Name],
		dbo.GetPhoneticName([token].[Name], 4) AS [PhoneticName],
		[token].[Start] AS [TokenStart],
		[token].[End] AS [TokenEnd]
	FROM @Tokens AS [token]
	WHERE LEN([token].[Name]) >= 2
	AND NOT EXISTS(
		SELECT *
		FROM @Tokens AS [source]
		WHERE LEN([source].[Name]) = 1
		AND [source].[Start] >= [token].[Start] 
		AND [source].[End] <= [token].[End])) AS [source]
WHERE [PhoneticName] > 0;

-- ������������ ����������
DECLARE @Addresses table (
	[Postcode] int null,
	[PostcodeIsDetected] int null,
	[RegionId] int null,
	[RegionTypeId] int null,
	[RegionName] nvarchar(max) null,
	[RegionPostcode] int null,
	[RegionIsLive] int null,
	[RegionTokenStart] int null,
	[RegionTokenEnd] int null,
	[RegionDistance] float null,
	[RegionTypeIsDetected] int not null,
	[RegionIsDetected] int not null,
	[AreaId] int null,
	[AreaTypeId] int null,
	--[AreaRegionId] int null,
	[AreaName] nvarchar(max) null,
	[AreaPostcode] int null,
	[AreaIsLive] int null,
	[AreaTokenStart] int null,
	[AreaTokenEnd] int null,
	[AreaDistance] float null,
	[AreaTypeIsDetected] int not null,
	[AreaIsDetected] int not null,
	[LocalityId] int null,
	[LocalityTypeId] int null,
	--[LocalityRegionId] int null,
	--[LocalityAreaId] int null,
	[LocalityName] nvarchar(max) null,
	[LocalityPostcode] int null,
	[LocalityIsLive] int null,
	[LocalityTokenStart] int null,
	[LocalityTokenEnd] int null,
	[LocalityDistance] float null,
	[LocalityTypeIsDetected] int not null,
	[LocalityIsDetected] int not null,
	[ThoroughfareId] int null,
	[ThoroughfareTypeId] int null,
	--[ThoroughfareRegionId] int null,
	--[ThoroughfareAreaId] int null,
	--[ThoroughfareLocalityId] int null,
	[ThoroughfareName] nvarchar(max) null,
	[ThoroughfarePostcode] int null,
	[ThoroughfareIsLive] int null,
	[ThoroughfareTokenStart] int null,
	[ThoroughfareTokenEnd] int null,
	[ThoroughfareDistance] float null,
	[ThoroughfareTypeIsDetected] int not null,
	[ThoroughfareIsDetected] int not null,
	[BuildingIndex] int null,
	[BuildingName] nvarchar(max) null,
	[BuildingPostcode] int null,
	[BuildingIsDetected] int null,
	[Relevance] float null);

-- ����� � ���������� �������� ��� ����� �� �����
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected] = 0,
	[RegionIsDetected] = 1,
	[AreaTypeIsDetected] = 0,
	[AreaIsDetected] = 0,
	[LocalityTypeIsDetected] = 0,
	[LocalityIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareIsDetected] = 0
FROM (
	SELECT
		[region].[Id] AS [RegionId],
		[region].[TypeId] AS [RegionTypeId],
		[token].[TokenStart] AS [RegionTokenStart],
		[token].[TokenEnd] AS [RegionTokenEnd],
		dbo.Levenstein([token].[Name], [region].[Name]) AS [RegionDistance]
	FROM (SELECT * FROM [dbo].[AddressRegions] WHERE [IsLive] = 1 OR [IsActual] = 1) AS [region]
	INNER JOIN @Names AS [token]
		ON [region].[PhoneticName] = [token].[PhoneticName]) AS [source]
WHERE [RegionDistance] <= @limitRegionDistance;

-- ���������� �������� � ������ �� �����
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[RegionId],
	[RegionTypeId],
	CASE WHEN [Start] > [RegionTokenStart] THEN [RegionTokenStart] ELSE [Start] END AS [RegionTokenStart],
	CASE WHEN [End] > [RegionTokenEnd] THEN [End] ELSE [RegionTokenEnd] END AS [RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected] = 1,
	[RegionIsDetected],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected]
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @regionLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Addresses AS [region]
	ON [type].[Id] = [region].[RegionTypeId]
	AND ([token].[Start] = [region].[RegionTokenEnd] + 1 OR [token].[End] = [region].[RegionTokenStart] - 1);

-- ����� ������� ��� ����� �� �����
DECLARE @Areas table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Areas
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[area].[Id],
		[area].[TypeId],
		[area].[RegionId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [area].[Name]) AS [AreaDistance]
	FROM (SELECT * FROM [dbo].[AddressAreas] WHERE [IsLive] = 1 OR [IsActual] = 1) AS [area]
	INNER JOIN @Names AS [token]
		ON [area].[PhoneticName] = [token].[PhoneticName]) AS [source]
WHERE [AreaDistance] <= @limitAreaDistance;

-- ����� ������� c ������ �� �����
INSERT INTO @Areas
SELECT
	[area].[Id],
	[area].[TypeId],
	[area].[RegionId],
	CASE WHEN [token].[Start] > [area].[TokenStart] THEN [area].[TokenStart] ELSE [token].[Start] END AS [TokenStart],
	CASE WHEN [token].[End] > [area].[TokenEnd] THEN [token].[End] ELSE [area].[TokenEnd] END AS [TokenEnd],
	[area].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @areaLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Areas AS [area]
	ON [type].[Id] = [area].[TypeId]
	AND ([token].[Start] = [area].[TokenEnd] + 1 OR [token].[End] = [area].[TokenStart] - 1);

-- ����� � ���������� �������� � �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	--[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[address].[RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	[address].[RegionTypeIsDetected],
	[address].[RegionIsDetected],
	[area].[Id] AS [AreaId],
	--[area].[RegionId] AS [AreaRegionId],
	[area].[TokenStart] AS [AreaTokenStart],
	[area].[TokenEnd] AS [AreaTokenEnd],
	[area].[Distance] AS [AreaDistance],
	[area].[TypeIsDetected] AS [AreaTypeIsDetected],
	[AreaIsDetected] = 1,
	[LocalityTypeIsDetected] = 0,
	[LocalityIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareIsDetected] = 0
FROM @Areas AS [area]
INNER JOIN @Addresses AS [address]
	ON [area].[RegionId] = [address].[RegionId]
	AND ([area].[TokenStart] > [address].[RegionTokenEnd] OR [area].[TokenEnd] < [address].[RegionTokenStart]);

-- ���������� �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	--[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[area].[RegionId] AS [RegionId],
	[RegionTypeIsDetected] = 0,
	[RegionIsDetected] = 0,
	[area].[Id] As [AreaId],
	--[area].[RegionId] AS [AreaRegionId],
	[area].[TokenStart] AS [AreaTokenStart],
	[area].[TokenEnd] AS [AreaTokenEnd],
	[area].[Distance] As [AreaDistance],
	[area].[TypeIsDetected] AS [AreaTypeIsDetected],
	[AreaIsDetected] = 1,
	[LocalityTypeIsDetected] = 0,
	[LocalityIsDetected] = 0,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareIsDetected] = 0
FROM @Areas AS [area];

-- ����� ��������� ������� ��� ����� �� �����
DECLARE @Localities table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[AreaId] int null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Localities
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[locality].[Id],
		[locality].[TypeId],
		[locality].[RegionId],
		[locality].[AreaId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [locality].[FormalName]) AS [Distance]
		FROM (SELECT * FROM [dbo].[AddressLocalities] WHERE [IsLive] = 1 OR [IsActual] = 1) AS [locality]
		INNER JOIN @Names AS [token]
			ON [locality].[PhoneticFormalName] = [token].[PhoneticName]) AS [source]
WHERE [Distance] <= @limitLocalityDistance;

INSERT INTO @Localities
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[locality].[Id],
		[locality].[TypeId],
		[locality].[RegionId],
		[locality].[AreaId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [locality].[OfficialName]) AS [Distance]
		FROM (SELECT * FROM [dbo].[AddressLocalities] WHERE [OfficialName] IS NOT NULL AND ([IsLive] = 1 OR [IsActual] = 1)) AS [locality]
		INNER JOIN @Names AS [token]
			ON [locality].[PhoneticOfficialName] = [token].[PhoneticName]) AS [source]
WHERE [Distance] <= @limitLocalityDistance;

-- ����� ��������� ������� � ������ �� �����
INSERT INTO @Localities
SELECT
	[locality].[Id],
	[locality].[TypeId],
	[locality].[RegionId],
	[locality].[AreaId],
	CASE WHEN [token].[Start] < [locality].[TokenStart] THEN [token].[Start] ELSE [locality].[TokenStart] END AS [TokenStart],
	CASE WHEN [token].[End] < [locality].[TokenEnd] THEN [locality].[TokenEnd] ELSE [token].[End] END AS [TokenEnd],
	[locality].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @localityLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Localities AS [locality]
	ON [type].[Id] = [locality].[TypeId]
	AND ([token].[End] = [locality].[TokenStart] - 1 OR [token].[Start] = [locality].[TokenEnd] + 1);

-- ����� � ���������� ��������, ������� � ��������� �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	--[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityId],
	--[LocalityRegionId],
	--[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[address].[RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	[address].[RegionTypeIsDetected],
	[address].[RegionIsDetected],
	[address].[AreaId],
	--[address].[AreaRegionId],
	[address].[AreaTokenStart],
	[address].[AreaTokenEnd],
	[address].[AreaDistance],
	[address].[AreaTypeIsDetected],
	[address].[AreaIsDetected],
	[locality].[Id],
	--[locality].[RegionId],
	--[locality].[AreaId],
	[locality].[TokenStart],
	[locality].[TokenEnd],
	[locality].[Distance],
	[locality].[TypeIsDetected],
	[LocalityIsDetected] = 1,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareIsDetected] = 0
FROM @Addresses AS [address]
INNER JOIN @Localities AS [locality]
		ON ([address].[RegionIsDetected] = 0
			AND [address].[AreaId] = [locality].[AreaId]
			AND ([address].[AreaTokenStart] > [locality].[TokenEnd] OR [address].[AreaTokenEnd] < [locality].[TokenStart]))
		OR (([address].[RegionId] = [locality].[RegionId]
				AND ([address].[RegionTokenStart] > [locality].[TokenEnd] OR [address].[RegionTokenEnd] < [locality].[TokenStart]))
			AND ([address].[AreaIsDetected] = 0
				OR ([address].[AreaId] = [locality].[AreaId]
					AND ([address].[AreaTokenStart] > [locality].[TokenEnd] OR [address].[AreaTokenEnd] < [locality].[TokenStart]))));

-- ���������� ��������� �������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityId],
	--[LocalityRegionId],
	--[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	[locality].[RegionId] AS [RegionId],
	[RegionTypeIsDetected] = 0,
	[RegionIsDetected] = 0,
	[locality].[AreaId] AS [AreaId],
	[AreaTypeIsDetected] = 0,
	[AreaIsDetected] = 0,
	[locality].[Id] As [LocalityId],
	--[locality].[RegionId] AS [LocalityRegionId],
	--[locality].[AreaId] AS [LocalityAreaId],
	[locality].[TokenStart] AS [LocalityTokenStart],
	[locality].[TokenEnd] AS [LocalityTokenEnd],
	[locality].[Distance] As [LocalityDistance],
	[locality].[TypeIsDetected] AS [LocalityTypeIsDetected],
	[LocalityIsDetected] = 1,
	[ThoroughfareTypeIsDetected] = 0,
	[ThoroughfareIsDetected] = 0
FROM @Localities AS [locality];

-- ����� �������������� ��� ����� �� �����
DECLARE @Thoroughfares table (
	[Id] int not null,
	[TypeId] int not null,
	[RegionId] int not null,
	[AreaId] int null, 
	[LocalityId] int null,
	[TokenStart] int not null,
	[TokenEnd] int not null,
	[Distance] float not null,
	[TypeIsDetected] bit not null);
INSERT INTO @Thoroughfares
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[thoroughfare].[Id],
		[thoroughfare].[TypeId],
		[thoroughfare].[RegionId],
		[thoroughfare].[AreaId],
		[thoroughfare].[LocalityId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [thoroughfare].[FormalName]) AS [Distance]
		FROM (SELECT * FROM [dbo].[AddressThoroughfares] WHERE [IsLive] = 1 OR [IsActual] = 1) AS [thoroughfare]
		INNER JOIN @Names AS [token]
			ON [thoroughfare].[PhoneticFormalName] = [token].[PhoneticName]) AS [source]
WHERE [Distance] <= @limitThoroughfareDistance;

INSERT INTO @Thoroughfares
SELECT
	[source].*,
	[TypeIsDetected] = 0
FROM (
	SELECT
		[thoroughfare].[Id],
		[thoroughfare].[TypeId],
		[thoroughfare].[RegionId],
		[thoroughfare].[AreaId],
		[thoroughfare].[LocalityId],
		[token].[TokenStart],
		[token].[TokenEnd],
		dbo.Levenstein([token].[Name], [thoroughfare].[OfficialName]) AS [Distance]
		FROM (SELECT * FROM [dbo].[AddressThoroughfares] WHERE [OfficialName] IS NOT NULL AND [IsLive] = 1 OR [IsActual] = 1) AS [thoroughfare]
		INNER JOIN @Names AS [token]
			ON [thoroughfare].[PhoneticOfficialName] = [token].[PhoneticName]) AS [source]
WHERE [Distance] <= @limitThoroughfareDistance;

-- ����� �������������� � ������ �� �����
INSERT INTO @Thoroughfares
SELECT
	[thoroughfare].[Id],
	[thoroughfare].[TypeId],
	[thoroughfare].[RegionId],
	[thoroughfare].[AreaId],
	[thoroughfare].[LocalityId],
	CASE WHEN [token].[Start] < [thoroughfare].[TokenStart] THEN [token].[Start] ELSE [thoroughfare].[TokenStart] END AS [TokenStart],
	CASE WHEN [token].[End] < [thoroughfare].[TokenEnd] THEN [thoroughfare].[TokenEnd] ELSE [token].[End] END AS [TokenEnd],
	[thoroughfare].[Distance],
	[TypeIsDetected] = 1
FROM [dbo].[AddressObjectTypes] AS [type]
INNER JOIN @Tokens AS [token]
	ON [type].[LevelId] = @thoroughfareLevelId
	AND [token].[Name] IN ([type].[ShortName], [type].[FullName])
INNER JOIN @Thoroughfares AS [thoroughfare]
	ON [type].[Id] = [thoroughfare].[TypeId]
	AND ([token].[End] = [thoroughfare].[TokenStart] - 1 OR [token].[Start] = [thoroughfare].[TokenEnd] + 1);

-- ����� � ���������� ��������, �������, ��������� ������� � ��������������
INSERT INTO @Addresses (
	[RegionId],
	[RegionTokenStart],
	[RegionTokenEnd],
	[RegionDistance],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	--[AreaRegionId],
	[AreaTokenStart],
	[AreaTokenEnd],
	[AreaDistance],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityId],
	--[LocalityRegionId],
	--[LocalityAreaId],
	[LocalityTokenStart],
	[LocalityTokenEnd],
	[LocalityDistance],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareId],
	--[ThoroughfareRegionId],
	--[ThoroughfareAreaId],
	--[ThoroughfareLocalityId],
	[ThoroughfareTokenStart],
	[ThoroughfareTokenEnd],
	[ThoroughfareDistance],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected])
SELECT
	ISNULL([address].[RegionId], [thoroughfare].[RegionId]) AS [RegionId],
	[address].[RegionTokenStart],
	[address].[RegionTokenEnd],
	[address].[RegionDistance],
	ISNULL([address].[RegionTypeIsDetected], 0) AS [RegionTypeIsDetected],
	ISNULL([address].[RegionIsDetected], 0) AS [RegionIsDetected],
	ISNULL([address].[AreaId], [thoroughfare].[AreaId]) AS [AreaId],
	--[address].[AreaRegionId],
	[address].[AreaTokenStart],
	[address].[AreaTokenEnd],
	[address].[AreaDistance],
	ISNULL([address].[AreaTypeIsDetected], 0) AS [AreaTypeIsDetected],
	ISNULL([address].[AreaIsDetected], 0) AS [AreaIsDetected],
	ISNULL([address].[LocalityId], [thoroughfare].[LocalityId]) AS [LocalityId],
	--[address].[LocalityRegionId],
	--[address].[LocalityAreaId],
	[address].[LocalityTokenStart],
	[address].[LocalityTokenEnd],
	[address].[LocalityDistance],
	ISNULL([address].[LocalityTypeIsDetected], 0) AS [LocalityTypeIsDetected],
	ISNULL([address].[LocalityIsDetected], 0) AS [LocalityIsDetected],
	[thoroughfare].[Id] AS [ThoroughfareId],
	--[thoroughfare].[RegionId] AS [ThoroughfareRegionId],
	--[thoroughfare].[AreaId] AS [ThoroughfareAreaId],
	--[thoroughfare].[LocalityId] AS [ThoroughfareLocalityId],
	[thoroughfare].[TokenStart] AS [ThoroughfareTokenStart],
	[thoroughfare].[TokenEnd] AS [ThoroughfareTokenEnd],
	[thoroughfare].[Distance] AS [ThoroughfareDistance],
	ISNULL([thoroughfare].[TypeIsDetected], 0) AS [ThoroughfareTypeIsDetected],
	CASE WHEN [thoroughfare].[Id] IS NULL THEN 0 ELSE 1 END AS [ThoroughfareIsDetected]
FROM @Addresses AS [address]
RIGHT JOIN @Thoroughfares AS [thoroughfare]
	ON ([address].[RegionIsDetected] = 0
		AND (([address].[AreaIsDetected] = 0
				AND [address].[LocalityId] = [thoroughfare].[LocalityId]
				AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))
			OR (([address].[AreaId] = [thoroughfare].[AreaId]
					AND ([address].[AreaTokenStart] > [thoroughfare].[TokenEnd] OR [address].[AreaTokenEnd] < [thoroughfare].[TokenStart]))
				AND ([address].[LocalityIsDetected] = 0
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))))
	OR ([address].[RegionId] = [thoroughfare].[RegionId]
		AND ([address].[RegionTokenStart] > [thoroughfare].[TokenEnd] OR [address].[RegionTokenEnd] < [thoroughfare].[TokenStart])
		AND (([address].[AreaIsDetected] = 0
				AND ([address].[LocalityIsDetected] = 0
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))
			OR ([address].[AreaId] = [thoroughfare].[AreaId]
				AND ([address].[AreaTokenStart] > [thoroughfare].[TokenEnd] OR [address].[AreaTokenEnd] < [thoroughfare].[TokenStart])
				AND ([address].[LocalityIsDetected] = 0
					OR ([address].[LocalityId] = [thoroughfare].[LocalityId]
						AND ([address].[LocalityTokenStart] > [thoroughfare].[TokenEnd] OR [address].[LocalityTokenEnd] < [thoroughfare].[TokenStart]))))));

-- ����� � ���������� ������� ��������
UPDATE [address]
SET [BuildingIndex] = [building].[Index],
	[BuildingName] = [building].[Name]
FROM (
	SELECT *,
		CASE WHEN [ThoroughfareIsDetected] = 1
			THEN [ThoroughfareTokenEnd]
			ELSE [LocalityTokenEnd]
		END + 1 AS [TokenEnd]
	FROM @Addresses 
	WHERE [ThoroughfareIsDetected] = 1 OR [LocalityIsDetected] = 1) AS [address]
CROSS APPLY (
	SELECT TOP(1) *
	FROM (
		SELECT
			[building].*,
			([building].[TokenStart] - [address].[TokenEnd]) AS [TokenDistance]
		FROM @Buildings AS [building]
		WHERE [building].[TokenStart] >= [address].[TokenEnd]) AS [building]
	WHERE [building].[TokenDistance] <= 1
	ORDER BY [TokenDistance]) AS [building]
WHERE [building].[TokenDistance] = 0
OR (([address].[RegionIsDetected] = 0 OR [address].[RegionTokenStart] <> [address].[TokenEnd])
	AND ([address].[AreaIsDetected] = 0 OR [address].[AreaTokenStart] <> [address].[TokenEnd])
	AND ([address].[ThoroughfareIsDetected] = 0
		OR [address].[LocalityIsDetected] = 0 OR [address].[LocalityTokenStart] <> [address].[TokenEnd]));

UPDATE @Addresses
SET [BuildingIsDetected] = 1,
	[BuildingPostcode] = [interval].[Postcode]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressBuildingsIntervals] AS [interval]
	ON [address].[LocalityIsDetected] = 1
	AND [address].[BuildingIndex] IS NOT NULL
	AND [interval].[ParentLevelId] = @localityLevelId
	AND [interval].[ParentId] = [address].[LocalityId]
	AND [address].[BuildingIndex] >= [interval].[StartIndex]
	AND [address].[BuildingIndex] <= [interval].[EndIndex];

UPDATE @Addresses
SET [BuildingIsDetected] = 1,
	[BuildingPostcode] = [interval].[Postcode]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressBuildingsIntervals] AS [interval]
	ON [address].[ThoroughfareIsDetected] = 1
	AND [address].[BuildingIndex] IS NOT NULL
	AND [interval].[ParentLevelId] = @thoroughfareLevelId
	AND [interval].[ParentId] = [address].[ThoroughfareId]
	AND [address].[BuildingIndex] >= [interval].[StartIndex]
	AND [address].[BuildingIndex] <= [interval].[EndIndex];

-- ����� � ���������� ����������� ������ ������
UPDATE @Addresses
SET	[RegionName] = [region].[Name],
	[RegionPostcode] = [region].[Postcode],
	[RegionIsLive] = [region].[IsLive]
FROM @Addresses AS [address] 
INNER JOIN [dbo].[AddressRegions] AS [region]
	ON [address].[RegionId] = [region].[Id];

UPDATE @Addresses
SET [AreaName] = [area].[Name],
	[AreaPostcode] = [area].[Postcode],
	[AreaIsLive] = [area].[IsLive]
FROM @Addresses AS [address] 
INNER JOIN [dbo].[AddressAreas] AS [area]
	ON [address].[AreaId] = [area].[Id];

UPDATE @Addresses
SET [LocalityName] = ISNULL([locality].[OfficialName], [locality].[FormalName]),
	[LocalityPostcode] = [locality].[Postcode],
	[LocalityIsLive] = [locality].[IsLive]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressLocalities] AS [locality]
	ON [address].[LocalityId] = [locality].[Id];

UPDATE @Addresses
SET [ThoroughfareName] = ISNULL([thoroughfare].[OfficialName], [thoroughfare].[FormalName]),
	[ThoroughfarePostcode] = [thoroughfare].[Postcode],
	[ThoroughfareIsLive] = [thoroughfare].[IsLive]
FROM @Addresses AS [address]
INNER JOIN [dbo].[AddressThoroughfares] AS [thoroughfare]
	ON [address].[ThoroughfareId] = [thoroughfare].[Id];

-- ����� � ���������� �������� ��������
UPDATE @Addresses
SET [Postcode] = ISNULL([postal].[Code], ISNULL([address].[BuildingPostcode], ISNULL([address].[ThoroughfarePostcode], ISNULL([address].[LocalityPostcode], ISNULL([address].[AreaPostcode], [address].[RegionPostcode]))))),
	[PostcodeIsDetected] = CASE WHEN [postal].[Code] IS NULL THEN 0 ELSE 1 END
FROM @Addresses AS [address]
OUTER APPLY (
	SELECT TOP(1) *
	FROM @Postcodes AS [postcode]
	WHERE ABS([postcode].[Code] - [address].[BuildingPostcode]) < 10
		OR ABS([postcode].[Code] - [address].[ThoroughfarePostcode]) < 10
		OR ABS([postcode].[Code] - [address].[LocalityPostcode]) < 10
		OR ABS([postcode].[Code] - [address].[AreaPostcode]) < 10
		OR ABS([postcode].[Code] - [address].[RegionPostcode]) < 10) AS [postal];

-- ������� �������������
DECLARE
	@typeDistance float = 0.2,
	@postcodeWeight float = 1,
	@regionWeight float = 1,
	@areaWeight float = 1,
	@localityWeight float = 1,
	@thoroughfareWeight float = 1;

UPDATE @Addresses
SET [Relevance] = [relevance].[Value]
FROM @Addresses AS [address]
CROSS APPLY (
	SELECT 1 - SUM([tmp].[Distance]) / SUM([tmp].[HasDistance]) AS [Value]
	FROM (
		SELECT
			1 AS [HasDistance],
			CASE WHEN [address].[PostcodeIsDetected] = 0
				THEN 1 * @postcodeWeight
				ELSE 0
			END AS [Distance]
		UNION ALL
		SELECT 
			1 AS [HasDistance],
			CASE WHEN [address].[RegionIsDetected] = 0
				THEN 1
				ELSE CASE WHEN [address].[RegionTypeIsDetected] = 0
						THEN [address].[RegionDistance] + @typeDistance
						ELSE [address].[RegionDistance] END
			END * @regionWeight AS [Distance]
		UNION ALL
		SELECT
			[area].[HasValue] AS [HasDistance],
			CASE WHEN [area].[HasValue] = 0
				THEN 0
				ELSE CASE WHEN [address].[AreaIsDetected] = 0
					THEN 1
					ELSE CASE WHEN [address].[AreaTypeIsDetected] = 0
						THEN [address].[AreaDistance] + @typeDistance
						ELSE [address].[AreaDistance] END
					END * @areaWeight
			END  AS [Distance]
		FROM (
			SELECT CASE WHEN [address].[AreaIsDetected] = 0
						AND [address].[AreaId] IS NULL
						AND ([address].[LocalityIsDetected] = 1 OR [address].[ThoroughfareIsDetected] = 1)
					THEN 0
					ELSE 1 END AS [HasValue]) AS [area]
		UNION ALL
		SELECT
			[locality].[HasValue] AS [HasDistance],
			CASE WHEN [locality].[HasValue] = 0
				THEN 0
				ELSE CASE WHEN [address].[LocalityIsDetected] = 0
					THEN 1
					ELSE CASE WHEN [address].[LocalityTypeIsDetected] = 0
						THEN [address].[LocalityDistance] + @typeDistance
						ELSE [address].[LocalityDistance] END
					END * @localityWeight
			END AS [Distance]
		FROM (
			SELECT CASE WHEN [address].[LocalityIsDetected] = 0
						AND [address].[LocalityId] IS NULL AND [address].[ThoroughfareIsDetected] = 1
					THEN 0
					ELSE 1 END AS [HasValue]) AS [locality]
		UNION ALL		
		SELECT
			1 AS [HasDistance],
			CASE WHEN [address].[ThoroughfareIsDetected] = 0
				THEN 1
				ELSE CASE WHEN [address].[ThoroughfareTypeIsDetected] = 0
					THEN [address].[ThoroughfareDistance] + @typeDistance 
					ELSE [address].[ThoroughfareDistance]
				END * @thoroughfareWeight
			END AS [Distance]) AS [tmp]) AS [relevance];

-- �������� ����������� �������
DELETE FROM [address]
FROM @Addresses AS [address]
INNER JOIN (
		SELECT [RegionId], [AreaId], [LocalityId], [ThoroughfareId], MAX([Relevance]) AS [Relevance]
		FROM @Addresses
		GROUP BY [RegionId], [AreaId], [LocalityId], [ThoroughfareId]) AS [source]
	ON (([address].[RegionId] IS NULL AND [source].[RegionId] IS NULL) OR [address].[RegionId] = [source].[RegionId])
	AND (([address].[AreaId] IS NULL AND [source].[AreaId] IS NULL) OR [address].[AreaId] = [source].[AreaId])
	AND (([address].[LocalityId] IS NULL AND [source].[LocalityId] IS NULL) OR [address].[LocalityId] = [source].[LocalityId])
	AND (([address].[ThoroughfareId] IS NULL AND [source].[ThoroughfareId] IS NULL) OR [address].[ThoroughfareId] = [source].[ThoroughfareId])
	AND [address].[Relevance] < [source].[Relevance];
	
DELETE FROM [address]
FROM @Addresses AS [address]
INNER JOIN (
		SELECT [RegionId], [AreaId], [LocalityId], [ThoroughfareId],
			MAX([RegionTypeIsDetected]
				+ [AreaTypeIsDetected]
				+ [LocalityTypeIsDetected]
				+ [ThoroughfareTypeIsDetected]
				+ CASE WHEN [BuildingIndex] IS NULL THEN 0 ELSE 1 END) AS [DetectedCount],
			MAX([RegionIsLive]
				+ [AreaIsLive]
				+ [LocalityIsLive]
				+ [ThoroughfareIsLive]) AS [LiveCount]
		FROM @Addresses
		GROUP BY [RegionId], [AreaId], [LocalityId], [ThoroughfareId]) AS [source]
	ON (([address].[RegionId] IS NULL AND [source].[RegionId] IS NULL) OR [address].[RegionId] = [source].[RegionId])
	AND (([address].[AreaId] IS NULL AND [source].[AreaId] IS NULL) OR [address].[AreaId] = [source].[AreaId])
	AND (([address].[LocalityId] IS NULL AND [source].[LocalityId] IS NULL) OR [address].[LocalityId] = [source].[LocalityId])
	AND (([address].[ThoroughfareId] IS NULL AND [source].[ThoroughfareId] IS NULL) OR [address].[ThoroughfareId] = [source].[ThoroughfareId])
	AND (([RegionTypeIsDetected]
		+ [AreaTypeIsDetected]
		+ [LocalityTypeIsDetected]
		+ [ThoroughfareTypeIsDetected]
		+ CASE WHEN [BuildingIndex] IS NULL THEN 0 ELSE 1 END) < [source].[DetectedCount]
			OR ([RegionIsLive]
				+ [AreaIsLive]
				+ [LocalityIsLive]
				+ [ThoroughfareIsLive]) < [source].[LiveCount]);

-- ������������ �������������� ����������
INSERT INTO @Results
SELECT TOP(@top)
	[Postcode],
	[PostcodeIsDetected],
	[RegionId],
	[RegionName],
	[RegionTypeIsDetected],
	[RegionIsDetected],
	[AreaId],
	[AreaName],
	[AreaTypeIsDetected],
	[AreaIsDetected],
	[LocalityId],
	[LocalityName],
	[LocalityTypeIsDetected],
	[LocalityIsDetected],
	[ThoroughfareId],
	[ThoroughfareName],
	[ThoroughfareTypeIsDetected],
	[ThoroughfareIsDetected],
	[Building],
	[BuildingIsDetected],
	[Relevance]
	--, [TokensDensity]
	--, [TokensLength]
	--, [TokensLive]
FROM (
	SELECT DISTINCT
		[address].[Postcode],
		[address].[PostcodeIsDetected],
		[address].[RegionId],
		[address].[RegionName],
		--[address].[RegionIsLive],
		--[address].[RegionTokenStart],
		--[address].[RegionTokenEnd],
		[address].[RegionTypeIsDetected],
		[address].[RegionIsDetected],
		[address].[AreaId],
		[address].[AreaName],
		--[address].[AreaIsLive],
		--[address].[AreaTokenStart],
		--[address].[AreaTokenEnd],
		[address].[AreaTypeIsDetected],
		[address].[AreaIsDetected],
		[address].[LocalityId],
		[address].[LocalityName],
		--[address].[LocalityIsLive],
		--[address].[LocalityTokenStart],
		--[address].[LocalityTokenEnd],
		[address].[LocalityTypeIsDetected],
		[address].[LocalityIsDetected],
		[address].[ThoroughfareId],
		[address].[ThoroughfareName],
		--[address].[ThoroughfareIsLive],
		--[address].[ThoroughfareTokenStart],
		--[address].[ThoroughfareTokenEnd],
		[address].[ThoroughfareTypeIsDetected],
		[address].[ThoroughfareIsDetected],
		[address].[BuildingName] AS [Building],
		ISNULL([address].[BuildingIsDetected], 0) AS [BuildingIsDetected],
		[address].[Relevance],
		(SELECT SUM([density].[Value])
		 FROM (
			SELECT ISNULL(CASE WHEN [RegionTokenEnd] < [AreaTokenStart] THEN [AreaTokenStart] - [RegionTokenEnd] ELSE [RegionTokenStart] - [AreaTokenEnd] END, 0) AS [Value] UNION ALL
			SELECT ISNULL(CASE WHEN [RegionTokenEnd] < [LocalityTokenStart] THEN [LocalityTokenStart] - [RegionTokenEnd] ELSE [RegionTokenStart] - [LocalityTokenEnd] END, 0) AS [Value] UNION ALL
			SELECT ISNULL(CASE WHEN [RegionTokenEnd] < [ThoroughfareTokenStart] THEN [ThoroughfareTokenStart] - [RegionTokenEnd] ELSE [RegionTokenStart] - [ThoroughfareTokenEnd] END, 0) AS [Value] UNION ALL
			SELECT ISNULL(CASE WHEN [AreaTokenEnd] < [LocalityTokenStart] THEN [LocalityTokenStart] - [AreaTokenEnd] ELSE [AreaTokenStart] - [LocalityTokenEnd] END, 0) AS [Value] UNION ALL
			SELECT ISNULL(CASE WHEN [AreaTokenEnd] < [ThoroughfareTokenStart] THEN [ThoroughfareTokenStart] - [AreaTokenEnd] ELSE [AreaTokenStart] - [ThoroughfareTokenEnd] END, 0) AS [Value] UNION ALL
			SELECT ISNULL(CASE WHEN [LocalityTokenEnd] < [ThoroughfareTokenStart] THEN [ThoroughfareTokenStart] - [LocalityTokenEnd] ELSE [LocalityTokenStart] - [ThoroughfareTokenEnd] END, 0) AS [Value]) AS [density]
		) AS [TokensDensity],
		ISNULL([RegionTokenEnd] - [RegionTokenStart], 0)
		+ ISNULL([AreaTokenEnd] - [AreaTokenStart], 0)
		+ ISNULL([LocalityTokenEnd] - [LocalityTokenStart], 0)
		+ ISNULL([ThoroughfareTokenEnd] - [ThoroughfareTokenStart], 0)
		AS [TokensLength],
		ISNULL([RegionIsLive], 0)
		+ ISNULL([AreaIsLive], 0)
		+ ISNULL([LocalityIsLive], 0)
		+ ISNULL([ThoroughfareIsLive], 0)
		AS [TokensLive]
	FROM @Addresses AS [address]) AS [result]
ORDER BY [Relevance] DESC,
	([PostcodeIsDetected]
	 + [RegionIsDetected] 
	 + [AreaIsDetected]
	 + [LocalityIsDetected] 
	 + [ThoroughfareIsDetected]) DESC,
	([RegionTypeIsDetected]
	 + [AreaTypeIsDetected]
	 + [LocalityTypeIsDetected]
	 + [ThoroughfareTypeIsDetected]) DESC,
	[BuildingIsDetected],
	[TokensDensity],
	[TokensLength] DESC,
	[TokensLive] DESC,
	[Postcode] DESC;

RETURN
END
GO