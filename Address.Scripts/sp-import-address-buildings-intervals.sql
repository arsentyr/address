-- �������� ��������� ������� ������� �� ���������� �������� �� �� ����

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressBuildingsIntervals')
	DROP PROCEDURE [dbo].[ImportAddressBuildingsIntervals];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressBuildingsIntervals]
	@fiasBuildingsIntervalsDbf nvarchar(max) -- ���� � dbf-����� �� ���� 
AS
BEGIN
SET NOCOUNT ON;

-- ������ �������� �������� �� ����
DECLARE
	@fiasEvenInterval int = 2,	-- ������
	@fiasOddInterval int = 3;	-- ��������

-- ������ ���������� �������� �� dbf-����� �� ����
DECLARE @intervals table (
	[Guid] uniqueidentifier not null,		-- guid ������
	[ParentGuid] uniqueidentifier not null, -- guid �������� ���������
	[ParentId] int null,					-- id �������� ���������
	[ParentLevelId] int null,				-- id ������ �������� ���������
	[StartIndex] smallint not null,			-- ��������� �������� ���������
	[EndIndex] smallint not null,			-- �������� �������� ���������
	[IsEven] bit null,						-- ������� ���������
	[Postcode] int null,					-- �������� ������ ���������
	[UpdateDate] date not null);			-- ���� ���������� ������ �� ���������
INSERT INTO @intervals (
	[Guid],
	[ParentGuid],
	[StartIndex],
	[EndIndex],
	[IsEven],
	[Postcode],
	[UpdateDate])
EXEC(N'
SELECT
	convert(uniqueidentifier, [interval].[HOUSEINTID]) AS [Guid],
	convert(uniqueidentifier, [interval].[AOGUID]) AS [ParentGuid],
	[interval].[INTSTART] AS [StartIndex],
	[interval].[INTEND] AS [EndIndex],
	CASE [interval].[INTSTATUS]
		WHEN ' + @fiasEvenInterval + ' THEN 1
		WHEN ' + @fiasOddInterval + ' THEN 0
		ELSE NULL
	END	AS [IsEven],
	CASE 
		WHEN [interval].[POSTALCODE] = 18865 THEN 188650
		ELSE [interval].[POSTALCODE]
	END AS [Postcode],
	[interval].[UPDATEDATE] AS [UpdateDate]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		SourceType=DBF;
		Deleted=No;
		BackgroundFetch=No;
		Exclusive=No;
		Null=No;'',
	''SELECT
		[HOUSEINTID],
		[AOGUID],
		[INTSTART],
		[INTEND],
		[INTSTATUS],
		[POSTALCODE],
		[UPDATEDATE]
	  FROM ' + @fiasBuildingsIntervalsDbf + ''') AS [interval]');

-- Id ������� �� �� ��
DECLARE
	@addressLocalityLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����'),	-- id ������ ���������� ������
	@addressThoroughfareLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������');	-- id ������ ��������������

-- ���������� �������� ��������� �� ��������� ������� � ��
UPDATE [interval]
SET [ParentId] = [locality].[Id],
	[ParentLevelId] = @addressLocalityLevelId
FROM @intervals AS [interval]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressLocalities]
		WHERE [IsLive] = 1) AS [locality] -- ������� ������� �� ����������� ��������� �������
	ON [interval].[ParentGuid] = [locality].[ObjectGuid]; 

UPDATE [interval]
SET	[ParentId] = [locality].[Id],
	[ParentLevelId] = @addressLocalityLevelId
FROM (
	SELECT *
	FROM @intervals
	WHERE [ParentId] IS NULL) AS [interval]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressLocalities] 
		WHERE [IsActual] = 1 AND [IsLive] = 0)  AS [locality] -- ����� ������� ������ �� ���������� ��������� �������
	ON [interval].[ParentGuid] = [locality].[ObjectGuid]; 

-- ���������� �������� ��������� �� �������������� � ��
UPDATE [interval]
SET	[ParentId] = [thoroughfare].[Id],
	[ParentLevelId] = @addressThoroughfareLevelId
FROM (
	SELECT *
	FROM @intervals
	WHERE [ParentId] IS NULL) AS [interval]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressThoroughfares]
		WHERE [IsLive] = 1)  AS [thoroughfare] -- ������� ������� �� ����������� ��������������
	ON [interval].[ParentGuid] = [thoroughfare].[ObjectGuid];

UPDATE [interval]
SET	[ParentId] = [thoroughfare].[Id],
	[ParentLevelId] = @addressThoroughfareLevelId
FROM (
	SELECT *
	FROM @intervals
	WHERE [ParentId] IS NULL) AS [interval]
INNER JOIN (
		SELECT *
		FROM [dbo].[AddressThoroughfares]
		WHERE [IsActual] = 1 AND [IsLive] = 0) AS [thoroughfare] -- ����� ������� ������ �� ���������� ��������������
	ON [interval].[ParentGuid] = [thoroughfare].[ObjectGuid]; 

-- ���������� ���������� � ���������� ������� �� ���������� �������� � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- ����������, ������������ � ��, ���������� ��������
UPDATE [dbo].[AddressBuildingsIntervals]
SET [ParentId] = [source].[ParentId],
	[ParentLevelId] = [source].[ParentLevelId],
	[StartIndex] = [source].[StartIndex],
	[EndIndex] = [source].[EndIndex],
	[IsEven] = [source].[IsEven],
	[Postcode] = [source].[Postcode],
	[UpdateDate] = [source].[UpdateDate]
FROM @intervals AS [source]
INNER JOIN [dbo].[AddressBuildingsIntervals] AS [destination]
	ON [source].[Guid] = [destination].[Guid];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- ����������, �������������� � ��, ��������� �������
INSERT INTO [dbo].[AddressBuildingsIntervals]
SELECT
	[Guid],
	[ParentId],
	[ParentLevelId],
	[StartIndex],
	[EndIndex],
	[IsEven],
	[Postcode],
	[UpdateDate]
FROM @intervals AS [source]
WHERE [Guid] NOT IN (SELECT [Guid] FROM [dbo].[AddressBuildingsIntervals]);

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @intervals);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ ���������� �������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ ���������� �������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO