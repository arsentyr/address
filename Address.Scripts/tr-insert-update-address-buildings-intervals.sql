-- ������� �� ���������� � ���������� ������� � ������� ���������� ��������

USE [work_db]
GO

-- �������� ������������� ��������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'TR' AND name = N'InsertUpdate_AddressBuildingsIntervals')
	DROP TRIGGER [dbo].[InsertUpdate_AddressBuildingsIntervals];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� ��������
CREATE TRIGGER [dbo].[InsertUpdate_AddressBuildingsIntervals]
ON [dbo].[AddressBuildingsIntervals]
AFTER INSERT, UPDATE 
AS
BEGIN
SET NOCOUNT ON;

-- ��� ����������� �������, ���� ���������� ������, �� �.�. ������ �������
IF EXISTS(
	SELECT *
	FROM inserted AS [in]
	INNER JOIN deleted AS [out]
		ON [in].[Guid] = [out].[Guid]
		AND [in].[UpdateDate] < [out].[UpdateDate])
BEGIN
	RAISERROR(N'��� ����������� �������, ���� ���������� ������, �� ����� ���� ������ �������.', 10, 1);
	ROLLBACK;
	RETURN
END

DECLARE @count int = (SELECT count(*) FROM inserted); -- ����� ����������� ���������� ��������

-- ������� ����� ��������� �� ��������� �������
SELECT @count -= count(*)
FROM inserted AS [interval]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������� �����')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressLocalities] AS [locality]
	WHERE [interval].[ParentId] = [locality].[Id]
	AND ([locality].[IsLive] = 1 OR [locality].[IsActual] = 1));

-- ������� ����� ��������� �� ��������������
SELECT @count -= count(*)
FROM inserted AS [interval]
WHERE [ParentLevelId] = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'��������������')
AND EXISTS(
	SELECT *
	FROM [dbo].[AddressThoroughfares] AS [thoroughfare]
	WHERE [interval].[ParentId] = [thoroughfare].[Id]
	AND ([thoroughfare].[IsLive] = 1 OR [thoroughfare].[IsActual] = 1));

-- �������� �� ��������� ����� ����������� ������� � ������ �� ���������
IF (@count <> 0)
BEGIN
	RAISERROR(N'������������ ������, ��� ����������� ���������� ��������, ������ ������������ � ���� ������ � ���� ������������ ��� �����������.', 10, 1);
	ROLLBACK;
	RETURN
END

RETURN
END
GO