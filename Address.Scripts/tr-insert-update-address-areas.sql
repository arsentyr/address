-- ������� �� ���������� � ���������� ������� � ������� �������

USE [work_db]
GO

-- �������� ������������� ��������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'TR' AND name = N'InsertUpdate_AddressAreas')
	DROP TRIGGER [dbo].[InsertUpdate_AddressAreas];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� ��������
CREATE TRIGGER [dbo].[InsertUpdate_AddressAreas]
ON [dbo].[AddressAreas]
AFTER INSERT, UPDATE 
AS
BEGIN
SET NOCOUNT ON;

-- ��� ����������� �������, ���� ���������� ������, �� �.�. ������ �������
IF EXISTS(
	SELECT *
	FROM inserted AS [in]
	INNER JOIN deleted AS [out]
		ON [in].[Guid] = [out].[Guid]
		AND [in].[UpdateDate] < [out].[UpdateDate])
BEGIN
	RAISERROR(N'��� ����������� �������, ���� ���������� ������, �� ����� ���� ������ �������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ������ ������ ����� ���, ���������� ��� ������
IF EXISTS(
	SELECT [TypeId]
	FROM inserted
	GROUP BY [TypeId]
	HAVING [TypeId] NOT IN (
		SELECT [type].[Id]
		FROM (
			SELECT [Id]
			FROM [dbo].[AddressObjectLevels]
			WHERE [Name] = N'�����') AS [level]
		INNER JOIN [dbo].[AddressObjectTypes] AS [type]
			ON [level].[Id] = [type].[LevelId]))
BEGIN
	RAISERROR(N'������ ������ ����� ���, ���������� ��� ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- �.�. �� ����� ������ �������������� ����������� ������, ����� ������� � ������
IF EXISTS(
	SELECT [ObjectGuid] 
	FROM [dbo].[AddressAreas]
	WHERE [IsLive] = 0 AND [IsActual] = 1
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ �������������� ����������� ������, � ������� � ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ����������� ������
DECLARE @areas table (
	[ObjectGuid] uniqueidentifier not null,
	[IsLive] bit not null,
	[Name] nvarchar(max) not null,
	[TypeId] int not null,
	[Postcode] int null,
	[RegionId] int not null
);
INSERT INTO @areas
SELECT
	[ObjectGuid],
	[IsLive],
	[Name],
	[TypeId],
	[Postcode],
	[RegionId]
FROM [dbo].[AddressAreas]
WHERE [IsLive] = 1;

-- �.�. �� ����� ������ ������������ ������, ����� ������� � ������
IF EXISTS(
	SELECT [ObjectGuid]
	FROM @areas
	GROUP BY [ObjectGuid]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'����� ���� �� ����� ������ ������������ ������, � ������� � ������.', 10, 1);
	ROLLBACK;
	RETURN
END

-- ��� �������, �� ����������� �������, ��� ������������ �.�. ����������
IF EXISTS(
	SELECT [RegionId]
	FROM @areas
	GROUP BY [RegionId], [Name], [TypeId]
	HAVING count(*) > 1)
BEGIN
	RAISERROR(N'��� �������, �� ����������� �������, ��� ������������ ������ ���� ����������.', 10, 1);
	ROLLBACK;
	RETURN
END
/* TODO: ��������� �������� ������� � ��
-- ��� �������, �� ����������� �������, ��� �������� ������ �.�. ����������
IF EXISTS(
	SELECT [Postcode] 
	FROM @areas
	WHERE [Postcode] IS NOT NULL
	GROUP BY [Postcode]
	HAVING count(*) > 1)
BEGIN
	SELECT [Postcode] 
	FROM @areas
	WHERE [Postcode] IS NOT NULL
	GROUP BY [Postcode]
	HAVING count(*) > 1;

	ROLLBACK;
	RETURN
END
*/
DECLARE @count int = (SELECT count(*) FROM inserted); -- ����� ����������� �������

-- ������� ����� ��������� �� ��������
SELECT @count -= count(*)
FROM inserted AS [area]
INNER JOIN [dbo].[AddressRegions] AS [region]
	ON [area].[RegionId] = [region].[Id]
	AND ([region].[IsLive] = 1 OR [region].[IsActual] = 1);

-- �������� �� ��������� ����� ����������� ������� � ������ �� ���������
IF (@count <> 0)
BEGIN
	RAISERROR(N'������������ ������, ��� ����������� �������, ������ ������������ � ���� ������ � ���� ������������ ��� �����������.', 10, 1);
	ROLLBACK;
	RETURN
END

RETURN
END
GO