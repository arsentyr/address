-- �������� ��������� ������� ������� � ������� �� �� ����

USE [work_db]
GO

-- �������� ������������ �������� ���������
IF EXISTS(SELECT * FROM sys.objects WHERE type = N'P' AND name = N'ImportAddressAreas')
	DROP PROCEDURE [dbo].[ImportAddressAreas];
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- �������� �������� ���������
CREATE PROCEDURE [dbo].[ImportAddressAreas]
	@fiasAddressObjectsDbf nvarchar(max) -- ���� � dbf-����� �� ���� 
AS
BEGIN
SET NOCOUNT ON;

-- ������ �������� �������� (��) �� ���� 
DECLARE @fiasAreaLevel int = 3;	-- ������� ������

-- ������ ������� �� dbf-����� �� ����
DECLARE @areas table (
	[Guid] uniqueidentifier not null,		-- guid ������ �� ��
	[ObjectGuid] uniqueidentifier not null,	-- guid ��
	[ParentGuid] uniqueidentifier not null,	-- guid �������� ��
	[RegionId] int null,					-- id ������� ��
	[TypeId] int null,						-- id ���� ��
	[TypeShortName] nvarchar(10) not null,	-- ������� ������������ ���� ��
	[Name] nvarchar(120) not null,			-- ���������� ������������ ��
	[OfficialName] nvarchar(120) null,		-- ����������� ������������ ��
	[Postcode] nvarchar(6) null,			-- �������� ������ ��
	[IsLive] bit not null,					-- ������� ������������ ��
	[IsActual] bit not null,				-- ������� ������������ ��
	[UpdateDate] date not null,				-- ���� ���������� ������ �� ��
	[PrevGuid] uniqueidentifier null);		-- guid ���������� ������ �� ��
INSERT INTO @areas (
	[Guid],
	[ObjectGuid],
	[ParentGuid],
	[TypeShortName],
	[Name],
	[OfficialName],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate],
	[PrevGuid])
EXEC(N'
SELECT
	convert(uniqueidentifier, [source].[AOID]) AS [Guid],
	convert(uniqueidentifier, [source].[AOGUID]) AS [ObjectGuid],
	convert(uniqueidentifier, [source].[PARENTGUID]) AS [ParentGuid],
	lower(dbo.Trim([source].[SHORTNAME])) AS [TypeShortName],
	dbo.Trim([source].[FORMALNAME]) AS [Name],
	CASE 
		WHEN [source].[OFFNAME] IS NULL
			OR [source].[OFFNAME] = [source].[FORMALNAME] THEN NULL
		ELSE dbo.Trim([source].[OFFNAME])
	END AS [OfficialName],
	[source].[POSTALCODE] AS [Postcode],
	[source].[LIVESTATUS] AS [IsLive],
	[source].[ACTSTATUS] AS [IsActual],
	convert(date, [source].[UPDATEDATE]) AS [UpdateDate],
	convert(uniqueidentifier, [source].[PREVID]) AS [PrevGuid]
FROM OPENROWSET(''MSDASQL'',
	''Driver={Microsoft dBase Driver (*.dbf)};
		BackgroundFetch=No;
		Deleted=No;
		Exclusive=No;
		Null=No;
		SourceType=DBF;'',
	''SELECT
		[AOID],
		[AOGUID],
		[PARENTGUID],
		[AOLEVEL],
		[SHORTNAME],
		[FORMALNAME],
		[OFFNAME],
		[POSTALCODE],
		[LIVESTATUS],
		[ACTSTATUS],
		[UPDATEDATE],
		[PREVID]
	  FROM ' + @fiasAddressObjectsDbf + '
	  WHERE [AOLEVEL] = ' + @fiasAreaLevel + ''') AS [source]');

-- �������� ������������� �������
IF EXISTS(
		SELECT *
		FROM @areas
		WHERE [OfficialName] IS NOT NULL) -- ���������� ������������ ������ ������ ��������� � �����������
	THROW 50000, N'������ ������� ��� �������, �.�. ������������� ������ �� ������ ��������.', 1;

-- Id ������� �� �� ��
DECLARE @addressAreaLevelId int = (SELECT [Id] FROM [dbo].[AddressObjectLevels] WHERE [Name] = N'�����'); -- id ������ ������ 

-- ���������� ���� ��
UPDATE [area]
SET [TypeId] = [type].[Id]
FROM @areas AS [area]
INNER JOIN (
		SELECT [Id], [ShortName]
		FROM [dbo].[AddressObjectTypes]
		WHERE [LevelId] = @addressAreaLevelId) AS [type]
	ON [area].[TypeShortName] = [type].[ShortName];

-- ���������� �������� �� �� �������� � ��
UPDATE [area]
SET [RegionId] = [region].[Id]
FROM @areas AS [area]
INNER JOIN (
	SELECT *
	FROM [dbo].[AddressRegions]
	WHERE [IsLive] = 1)  AS [region] -- ������� ������� �� ����������� ��������
	ON [area].[ParentGuid] = [region].[ObjectGuid]; 

UPDATE [area]
SET [RegionId] = [region].[Id]
FROM (
	SELECT *
	FROM @areas
	WHERE [RegionId] IS NULL AND [IsLive] = 0) AS [area]
INNER JOIN (
	SELECT *
	FROM [dbo].[AddressRegions]
	WHERE [IsActual] = 1 AND [IsLive] = 0) AS [region]
	ON [area].[ParentGuid] = [region].[ObjectGuid]; -- ����� ������� ������ �� ���������� ��������

-- ���������� ���������� � ���������� ������� � ������� � ��
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
BEGIN TRANSACTION;

-- �����������, ����������������� � �� ����, �������� �������� ������������ ������
UPDATE [dbo].[AddressAreas]
SET [IsLive] = 0 -- �������� � 0 ������� ������������ ��,
FROM [dbo].[AddressAreas] AS [destination] -- ��� ������� �� ��, �������
INNER JOIN @areas AS [source]
	ON [destination].[ObjectGuid] = [source].[ObjectGuid] -- ����� ����� ������ �� �� �
	AND [destination].[Guid] <> [source].[Guid] -- guid ������� �� ��������� �
	AND [destination].[IsLive] = 1 AND [source].[IsLive] = 1 -- ��� �� �������� ������������ �
	AND [source].[PrevGuid] IS NOT NULL
	AND [destination].[Guid] = [source].[PrevGuid]; -- ������ �� �� � ��, �������� ���������� ������������ ����� ������

PRINT N'����������: ' + str(@@ROWCOUNT);

-- ����������, ������������ � ��, ��������� �������
UPDATE [dbo].[AddressAreas]
SET [ObjectGuid] = [source].[ObjectGuid],
	[RegionId] = [source].[RegionId],
	[TypeId] = [source].[TypeId],
	[Name] = [source].[Name] ,
	[Postcode] = [source].[Postcode],
	[IsLive] = [source].[IsLive],
	[IsActual] = [source].[IsActual],
	[UpdateDate] = [source].[UpdateDate]
FROM [dbo].[AddressAreas] AS [destination]
INNER JOIN @areas AS [source]
	ON [destination].[Guid] = [source].[Guid];

DECLARE @updated int = @@ROWCOUNT; -- ����� ���������� �������
PRINT N'���������: ' + str(@updated);

-- ����������, �������������� � ��, �������
INSERT INTO [dbo].[AddressAreas] (
	[Guid],
	[ObjectGuid],
	[RegionId],
	[TypeId],
	[Name],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate])
SELECT
	[Guid],
	[ObjectGuid],
	[RegionId],
	[TypeId],
	[Name],
	[Postcode],
	[IsLive],
	[IsActual],
	[UpdateDate]
FROM @areas AS [source]
WHERE NOT EXISTS(
	SELECT *
	FROM [dbo].[AddressAreas] AS [destination]
	WHERE [destination].[Guid] = [source].[Guid]);

DECLARE @inserted int = @@ROWCOUNT; -- ����� ����������� �������
PRINT N'���������: ' + str(@inserted);

-- �������� �����, ������������� �������, �� ��������� ����� ����������� � ����������� �������
DECLARE @count int = (SELECT count(*) FROM @areas);
PRINT N'�����: ' + str(@count);

IF (@count - @updated - @inserted = 0) BEGIN
	COMMIT TRANSACTION; -- �������� ���������� ����������
	PRINT N'������ ������� �������� �������.';
END
ELSE BEGIN
	ROLLBACK TRANSACTION; -- �����, ����� ����������
	THROW 50000, N'������ ������� ��� �������, �.�. ����� ������������� ������� �� ����� ����� ����������� � ����������� �������.', 1;
END

END
GO